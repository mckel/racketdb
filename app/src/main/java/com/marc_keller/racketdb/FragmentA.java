/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;


import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.HeaderViewListAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ListView;

import java.util.List;


/**
 * \brief Class of main racket list overview page.
 *
 * A simple {@link android.app.Fragment} subclass. Contains list of all rackets and information
 * about the last stringjob (date and used tension). Through this page, new string jobs can be
 * created.
 */
public class FragmentA extends Fragment {

    // The main view of the class
    private View viewFragmentA;
    // DbHelper is used to manage the database.
    private RacketDbHelper racketDbHelper;
    // The adapter that binds our data to the ListView
    private RacketListAdapter racketListAdapter;
    // Save filter visibility
    private int filterState = View.GONE;

    public FragmentA() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //inflate view, but don't return jet
        viewFragmentA = inflater.inflate(R.layout.fragment_a, container, false);


        // Populate racket ListView from database
        populateRacketListView(inflater);

        // Setup filter
        setUpFilter();

        // Set filter state
        viewFragmentA.findViewById(R.id.filter_rackets).setVisibility(filterState);

        // Setup fab
        setUpFAB();

        // Register Click listener
        registerListClickCallback();

        // Return the view for this fragment
        return viewFragmentA;
    }

    @Override
    public void onResume() {
        super.onResume();
        //do the query again every time on resume
        RacketDbHelper dbHelper = new RacketDbHelper(getActivity());
        Cursor c = dbHelper.getAllRacketsAlphabetic();
        racketListAdapter.changeCursor(c);
        dbHelper.close();
    }

    @Override
    public void onPause() {
        super.onPause();
        racketListAdapter.notifyDataSetInvalidated();
        racketListAdapter.changeCursor(null);
/*
        // close keyboard
        EditText etFilter = (EditText) getActivity().findViewById(R.id.filter_rackets);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etFilter.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
*/
    }

    @Override
    public void onStop() {
        super.onStop();
        filterState = getActivity().findViewById(R.id.filter_rackets).getVisibility();
    }


    /**
     * Methods
     */
    public void populateRacketListView(LayoutInflater inflater) {

        // Set up the dbHelper to manage the database.
        racketDbHelper = new RacketDbHelper(getActivity());

        // Get all entries from database and create cursor
        Cursor cursor = racketDbHelper.getAllRacketsAlphabetic();

        // Create adapter to lay columns of the DB onto element in the UI
        racketListAdapter = new RacketListAdapter(
                getActivity(),          // Contest
                cursor,                 // cursor (contains db data to map)
                0                  // flag
        );
        cursor.close();

        // First, add footer
        View footer = inflater.inflate(R.layout.listview_rackets_footer, null);
        ListView racketLV = (ListView) viewFragmentA.findViewById(R.id.lv_rackets);
        racketLV.addFooterView(footer, null, false);

        // Associate the (now empty) adapter with the ListView.
        try {
            racketLV.setAdapter(racketListAdapter);
        } catch (Exception e) {
            if (racketLV == null) {
            }
            e.printStackTrace();
        }

        // Filter setup
        racketLV.setTextFilterEnabled(true);

    }


    private void setUpFilter() {
        EditText etFilter = (EditText) viewFragmentA.findViewById(R.id.filter_rackets);

        etFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                ListView lv = (ListView) viewFragmentA.findViewById(R.id.lv_rackets);

                RacketListAdapter filterAdapter = (RacketListAdapter) ((HeaderViewListAdapter)lv.getAdapter()).getWrappedAdapter();
                filterAdapter.getFilter().filter(editable.toString());
            }
        });
        racketListAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence charSequence) {
                return racketDbHelper.getAllRacketsnameContain(charSequence.toString().trim());
            }
        });

    }

    private void setUpFAB() {

        final FloatingActionButton arFab = (FloatingActionButton) viewFragmentA.findViewById(R.id.fab_addracket);

        arFab.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                showAddRacketDialog(getActivity().getCurrentFocus());
            }
        });
    }

    // show dialog to add a racket
    public void showAddRacketDialog(View v) {
        FragmentManager fmManager = getActivity().getSupportFragmentManager();
        AddRacketDialog d = new AddRacketDialog();
        d.show(fmManager, "AddRacketDialog");
    }


    /**
     * OnClickListeners
     */


    private void registerListClickCallback() {
        final ListView racketList = (ListView) viewFragmentA.findViewById(R.id.lv_rackets);
        //final com.marc_keller.racketdb.ListView racketList = (com.marc_keller.racketdb.ListView) viewFragmentA.findViewById(R.id.lv_rackets);

        // short click
        racketList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked,
                                    int position, long idDB) {
                /*
                Toast.makeText(getActivity(), "Item clicked (Id = " + Long.toString(idDB) + " Pos= " + position + ")", Toast.LENGTH_SHORT).show();
                Racket r = racketDbHelper.getRacket((int) idDB);
                Toast.makeText(getActivity(), "Owner: " + r.getOwner() + " , Model: " + r.getModel(), Toast.LENGTH_LONG).show();
                */

                // Start Racket Overview Dialog

                // DialogFragment.show() will take care of adding the fragment
                // in a transaction.  We also want to remove any currently showing
                // dialog, so make our own transaction and take care of that here.
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("RacketSummaryDialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog
                DialogFragment d = RacketSummaryDialog.newInstance((int) idDB);
                d.show(ft, "RacketSummaryDialog");
            }
        });

        // long list click
        racketList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View viewClicked,
                                           int position, long idDB) {

                // Start Edit Dialog

                // DialogFragment.show() will take care of adding the fragment
                // in a transaction.  We also want to remove any currently showing
                // dialog, so make our own transaction and take care of that here.
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("EditDialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog
                DialogFragment d = EditDialog.newInstance((int) idDB);
                d.show(ft, "EditDialog");

                return true;
            }
        });

    }


    public static class EditDialog extends AppCompatDialogFragment {

        int idDB;

        /**
         * Create a new instance of MyDialogFragment, providing "id"
         * as an argument.
         */
        static EditDialog newInstance(int id) {
            EditDialog d = new EditDialog();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("racketID", id);
            d.setArguments(args);

            return d;
        }


        @Override
        public AppCompatDialog onCreateDialog(Bundle paramBundle) {
            super.onCreateDialog(paramBundle);
            // Get argument
            idDB = getArguments().getInt("racketID");


            // Use Builder to customize dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            // Dialog title
            builder.setTitle(R.string.dialogtitle_edit);

            // Set message
            builder.setMessage(R.string.edit_message);


            // Cancel Button
            DialogInterface.OnClickListener listenerNegative = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            };
            builder.setNegativeButton(R.string.String_Cancel, listenerNegative);


            // Next Button
            DialogInterface.OnClickListener listenerPositive = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInt, int paramInt) {

                    RacketDbHelper dbHelper = new RacketDbHelper(getActivity());

                    // Get corresponding StringJobs
                    Cursor cStringJobs = dbHelper.getAllStringJobRacket(idDB);
                    int count = cStringJobs.getCount();
                    if (count != 0) {
                        cStringJobs.moveToFirst();
                        // Delete StringJobs
                        for (int k = 1; k <= count; k++) {
                            dbHelper.deleteStringjob(cStringJobs.getInt(cStringJobs.getColumnIndex(RacketDbHelper.KEY_UID)));
                            cStringJobs.moveToNext();
                        }

                    }

                    // Delete Racket
                    dbHelper.deleteRacket(idDB);

                    // UPDATE listview
                    ListView racketLV = (ListView) getActivity().findViewById(R.id.lv_rackets);
                    RacketListAdapter racketListAdapter = (RacketListAdapter)((HeaderViewListAdapter)racketLV.getAdapter()).getWrappedAdapter();
                    Cursor newCursor = dbHelper.getAllRacketsAlphabetic();
                    racketListAdapter.changeCursor(newCursor);
                    racketListAdapter.notifyDataSetChanged();

                    // Spinner Drop down elements
                    Spinner spinner = (Spinner) getActivity().findViewById(R.id.spinnerHistory);
                    ArrayAdapter<String> dataAdapter = (ArrayAdapter<String>) spinner.getAdapter();
                    List<String> lables = dbHelper.getAllRacketsSpinner();
                    dataAdapter.clear();
                    if (lables != null) {
                        for (String str : lables) {
                            dataAdapter.insert(str, dataAdapter.getCount());
                        }
                    }
                    if (dataAdapter.getItem(0).equals(getResources().getString(R.string.pleaseAddRacket))) {
                        spinner.setEnabled(false);
                        spinner.setClickable(false);
                    } else {
                        spinner.setEnabled(true);
                        spinner.setClickable(true);
                    }
                    dataAdapter.notifyDataSetChanged();

                    // Update plots
                    FragmentB.updatePlots();

                    // Infrom User
                    Toast.makeText(getActivity(), getResources().getString(R.string.racketDeleted), Toast.LENGTH_SHORT).show();

                }
            };
            builder.setPositiveButton(R.string.delet_String, listenerPositive);

            // return dialog
            return builder.create();
        }
    }


}


