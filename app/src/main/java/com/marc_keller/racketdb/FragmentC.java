/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;


import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;


/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class FragmentC extends Fragment {

    public FragmentC() {
        // Required empty public constructor
    }

    private View viewFragmentC;
    // DbHelper is used to manage the database.
    private RacketDbHelper racketDbHelper;
    // The adapter that binds our data to the ListView
    private StringjobListAdapter stringjobListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //inflate layout, but don't return view jet
        viewFragmentC = inflater.inflate(R.layout.fragment_c, container,false);

        // Populate racket ListView from database
        populateStringjobtListView(inflater);

        // Register Click listener
        registerListClickCallback();

        // Return the view for this fragment
        return viewFragmentC;
    }

    @Override
    public void onResume(){
        super.onResume();
        //do the query again every time on resume
        Cursor c = racketDbHelper.getAllStringjobs();
        stringjobListAdapter.changeCursor(c);
    }

    @Override
    public void onPause(){
        super.onPause();
        stringjobListAdapter.notifyDataSetInvalidated();
        stringjobListAdapter.changeCursor(null);
    }

    /**
     * Methods
     */
    private void populateStringjobtListView(LayoutInflater inflater) {

        // Set up the dbHelper to manage the database.
        racketDbHelper = new RacketDbHelper(getActivity());

        // Get all entries from database and create cursor
        Cursor cursor = racketDbHelper.getAllRacketsAlphabetic();

        // Allow activity to manage lifetime of the cursor
        // DEPRICATED! Runs on UI thread: Ok for small/short queries
        //getActivity().startManagingCursor(cursor);

        // Create adapter to lay columns of the DB onto element in the UI
        stringjobListAdapter = new StringjobListAdapter(
                getActivity(),          // Contest
                cursor,                 // cursor (contains db data to map)
                0                       // flag
        );
        cursor.close();

        // First, add footer
        View footer = inflater.inflate(R.layout.listview_rackets_footer, null);
        ListView stringjobLV = (ListView) viewFragmentC.findViewById(R.id.lv_stringjobs);
        stringjobLV.addFooterView(footer, null, false);

        // Associate the (now empty) adapter with the ListView.
        try {
            stringjobLV.setAdapter(stringjobListAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * OnClickListeners
     */

    private void registerListClickCallback() {
        final ListView sjList = (ListView) viewFragmentC.findViewById(R.id.lv_stringjobs);
        // short click
        sjList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked,
                                    int position, long idDB) {
            }
        });

        // long list click
        sjList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View viewClicked,
                                           int position, long idDB) {



                // Spinner Drop down elements
                Spinner spinner = (Spinner) getActivity().findViewById(R.id.spinnerHistory);
                ArrayAdapter<String> dataAdapter = (ArrayAdapter<String>) spinner.getAdapter();
                List<String> lables = racketDbHelper.getAllRacketsSpinner();
                dataAdapter.clear();
                if (lables != null){
                    for (String str : lables) {
                        dataAdapter.insert(str, dataAdapter.getCount());
                    }
                }
                if(dataAdapter.getItem(0).equals(getResources().getString(R.string.pleaseAddRacket))){
                    spinner.setEnabled(false);
                    spinner.setClickable(false);
                } else {
                    spinner.setEnabled(true);
                    spinner.setClickable(true);
                }
                dataAdapter.notifyDataSetChanged();

                // Update Plots
                FragmentB.updatePlots();





                // Start Edit Dialog

                // DialogFragment.show() will take care of adding the fragment
                // in a transaction.  We also want to remove any currently showing
                // dialog, so make our own transaction and take care of that here.
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("Edit2Dialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog
                AppCompatDialogFragment d = Edit2Dialog.newInstance((int) idDB);
                d.show(ft, "Edit2Dialog");

                return true;
            }
        });
    }






    public static class Edit2Dialog extends AppCompatDialogFragment {

        int idDB;

        /**
         * Create a new instance of MyDialogFragment, providing "id"
         * as an argument.
         */
        static Edit2Dialog newInstance(int id) {
            Edit2Dialog d = new Edit2Dialog();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("stringjobID", id);
            d.setArguments(args);

            return d;
        }


        @Override
        public AppCompatDialog onCreateDialog(Bundle paramBundle) {
            super.onCreateDialog(paramBundle);

            // Get argument
            idDB = getArguments().getInt("stringjobID");

            // Use Builder to customize dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            // Dialog title
            builder.setTitle(R.string.dialogtitle_edit2);

            // Set Message
            builder.setMessage(R.string.edit2_message);


            // Cancel Button
            DialogInterface.OnClickListener listenerNegative = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            };
            builder.setNegativeButton(R.string.String_Cancel, listenerNegative);


            // Next Button
            DialogInterface.OnClickListener listenerPositive = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInt, int paramInt) {

                    RacketDbHelper dbHelper = new RacketDbHelper(getActivity());


                    // Delete Stringjob
                    dbHelper.deleteStringjob(idDB);

                    // UPDATE listview
                    ListView stringjobLV = (ListView) getActivity().findViewById(R.id.lv_stringjobs);
                    StringjobListAdapter sjLA = (StringjobListAdapter) ((HeaderViewListAdapter)stringjobLV.getAdapter()).getWrappedAdapter();

                    Cursor newCursor = dbHelper.getAllStringjobs();
                    sjLA.changeCursor(newCursor);
                    sjLA.notifyDataSetChanged();


                    // Spinner Drop down elements
                    Spinner spinner = (Spinner) getActivity().findViewById(R.id.spinnerHistory);
                    ArrayAdapter<String> dataAdapter = (ArrayAdapter<String>) spinner.getAdapter();
                    List<String> lables = dbHelper.getAllRacketsSpinner();
                    dataAdapter.clear();
                    if (lables != null){
                        for (String str : lables) {
                            dataAdapter.insert(str, dataAdapter.getCount());
                        }
                    }
                    if(dataAdapter.getItem(0).equals(getResources().getString(R.string.pleaseAddRacket))){
                        spinner.setEnabled(false);
                        spinner.setClickable(false);
                    } else {
                        spinner.setEnabled(true);
                        spinner.setClickable(true);
                    }
                    dataAdapter.notifyDataSetChanged();

                    // Update plots
                    FragmentB.updatePlots();

                    // Infrom User
                    Toast.makeText(getActivity(),getResources().getString(R.string.stringjobDeleted),Toast.LENGTH_SHORT).show();

                }
            };
            builder.setPositiveButton(R.string.delet_String, listenerPositive);

            // return dialog
            return builder.create();
        }
    }





}
