Privacy policy

General

RacketDB is developed by one main developer, with few people supporting this open-source project occasionally.

Privacy Policy ("Policy") describes how information obtained from users is collected, used and disclosed.

By using RacketDB, you agree that your personal information will be handled as described in this Policy.

The app has no internet access, so it is not sending anything outside the device whatsoever.

Information being collected

RacketDB does not collect any personal identifiable information.

The `READ_EXTERNAL_STORAGE` permission is used for restoring the RacketDB database from a backup from the users' device.

The `WRITE_EXTERNAL_STORAGE` permission is used for backing up the RacketDB database to the users' device.

Changes to the Policy

If the Policy changes, the modification date below will be updated. The Policy may change from time to time, so please be sure to check back periodically.

Last modified: 20 October, 2023.

Contact

If you have any questions about the Policy, please contact us via racketdb@gmail.com
