/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;


import android.graphics.Color;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.androidplot.pie.PieChart;
import com.androidplot.pie.PieRenderer;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.androidplot.ui.Anchor;
import com.androidplot.ui.HorizontalPositioning;
import com.androidplot.ui.VerticalPositioning;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BarRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.PointLabeler;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//import android.widget.Spinner;


/**
 * A simple {@link android.app.Fragment} subclass.
 *
 */
public class FragmentB extends Fragment {

    private static View view;
    private AppCompatSpinner spinner;
    private ArrayAdapter<String> dataAdapter;
    private RacketDbHelper dbHelper;
    private static int itemListID;
    private List<String> labels;
    private List<Integer> stringJobs;
    private List<Integer> stringJobsSixMonth;
    private List<Integer> racketNumbers;


    public FragmentB() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // inflate view
        view = inflater.inflate(R.layout.fragment_b,container,false);

        // Spinner
        spinner = (AppCompatSpinner) view.findViewById(R.id.spinnerHistory);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                itemListID = (int) id; //starts at 0!
                updatePlots();
                /*
                // On selecting a spinner item
                String label = adapterView.getItemAtPosition(position).toString();
                // Showing selected spinner item
                Toast.makeText(adapterView.getContext(), "You selected: " + label,
                        Toast.LENGTH_LONG).show();
                */
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        // database handler
        dbHelper = new RacketDbHelper(getActivity());

        // Get initial data
        // Spinner Drop down elements
        labels = dbHelper.getAllRacketsSpinner();
        // Stringjobs List
        stringJobs = dbHelper.getAllStringJobsSpinner(itemListID);
        // Last six month
        stringJobsSixMonth = dbHelper.getMonthlyStringJobsSpinnerSixMonth(itemListID);

        // number of all rackets of a specific model and owner
        racketNumbers = dbHelper.getRacketNumbersNameOwner(itemListID);


        // Initialize Spinner
        loadSpinnerData();

        // Initialize Plots
        initializePlot1();
        initializePlot2();
        initializePlot3();

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_b, container, false);
        return view;
    }


    /**
     * Methods
     */
    public void loadSpinnerData() {

        // Spinner Drop down elements
        // List<String> labels = dbHelper.getAllRacketsSpinner();

        // Creating adapter for spinner
        //dataAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, labels);
        dataAdapter = new ArrayAdapter<String>(getActivity(),R.layout.multiple_spinner_item,labels);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
        //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        // if empty
        if(dataAdapter.getItem(0).equals(getResources().getString(R.string.pleaseAddRacket))){
            spinner.setEnabled(false);
            spinner.setClickable(false);
        } else {
            spinner.setEnabled(true);
            spinner.setClickable(true);
        }

    }


    private void initializePlot1() {

        // initialize our XYPlot reference
        XYPlot plot1 = (XYPlot) view.findViewById(R.id.plotCount);

        // Is there any data?
        boolean noData = (stringJobs.equals(new ArrayList<Integer>(Collections.nCopies(stringJobs.size(),0))));

        // // Modify entry of zero stringjob rackets
        List<Float> plot1Data = new ArrayList<Float>();
        Float tmp;
        for (Integer i = 0; i < stringJobs.size(); i++) {

            tmp = (float) stringJobs.get(i);
            if (tmp == 0.0 && i!=0 && i != stringJobs.size() -1) {
                tmp = (float) 0.05;
            }
            plot1Data.add(tmp);
        }

        // Turn above array into XYSeries
        // (No conversion needed, since stringJobs is already an arraylist)
        XYSeries stringJobsSeries = new SimpleXYSeries(
                plot1Data,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                "Stringjob Series"
        );

        // Create formatter for bar plot
        //ContextCompat.getColor(context, R.color.color_name)
        BarFormatter bf1 = new BarFormatter(ContextCompat.getColor(requireContext(), R.color.green), Color.TRANSPARENT);
        //BarFormatter bf1 = new BarFormatter(getResources().getColor(R.color.green), Color.TRANSPARENT);

        // Add label (numbers) to bars
        bf1.setPointLabelFormatter(new PointLabelFormatter(Color.WHITE, 0f, 50f));
        bf1.setPointLabeler(new PointLabeler() {
            @Override
            public String getLabel(XYSeries series, int index) {
                int yValueRounded = Math.round((float) series.getY(index));
                if (yValueRounded == 0) {
                    return "";
                } else {
                    return String.valueOf(Math.round((float) series.getY(index)));
                }
            }
        });

        // Add data series to plot
        plot1.clear();
        plot1.addSeries(stringJobsSeries, bf1);
        plot1.redraw();

        // Plot Margins and Padding
        plot1.setPlotMargins(10, 0, 10, 30);
        plot1.setPlotPadding(20, 10, 20, 10);

        // Graph Marings and Padding
        plot1.getGraph().setMargins(0,0,0,0); // space between axis and label
        plot1.getGraph().setPadding(0,0,0,110); // (old: -10f)

        // Determine and set ticks interval on y-axis (Adapts to max number of string jobs.)
        plot1.setRangeStep(StepMode.INCREMENT_BY_VAL, Math.floor(((double) Collections.max(stringJobs)) / 6d) + 1);

        // Position of x-axis label. (Did not working via xml.)
        plot1.getDomainTitle().position(
           0, HorizontalPositioning.ABSOLUTE_FROM_CENTER,
           0, VerticalPositioning.ABSOLUTE_FROM_BOTTOM,
            Anchor.BOTTOM_MIDDLE);

        // Fix ranges of y-axis if plot is empty
        if (!noData) {
            plot1.setRangeBoundaries(0, 0, BoundaryMode.AUTO); // Auto: values don't apply
        } else {
            plot1.setRangeBoundaries(0,0.5, BoundaryMode.FIXED);
        }

        // Setup Bars
        if (!noData) { // only works with data in plot
            BarRenderer barRenderer = plot1.getRenderer(BarRenderer.class);
            barRenderer.setBarOrientation(BarRenderer.BarOrientation.SIDE_BY_SIDE);
            barRenderer.setBarGroupWidth(BarRenderer.BarGroupWidthMode.FIXED_GAP, PixelUtils.dpToPix(20));
        }

        // Remove stuff
        plot1.getLayoutManager().remove(plot1.getTitle());

        // Configure custom number Format !! (delete 0 and max on x-axis)
        plot1.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).
                setFormat(new NumberFormat() {
                    @Override
                    public StringBuffer format(double value, StringBuffer buffer, FieldPosition field) {
                        if (value == 0 || value == stringJobs.size() - 1) {
                            return new StringBuffer("");
                        } else {
                            //plot1.setRangeValueFormat(new DecimalFormat("#"));
                            DecimalFormat nf = new DecimalFormat("#");

                            return new StringBuffer(nf.format(racketNumbers.get((int) value)));
                        }
                    }

                    @Override
                    public StringBuffer format(long l, StringBuffer stringBuffer, FieldPosition fieldPosition) {
                        return null;
                    }

                    @Override
                    public Number parse(String s, ParsePosition parsePosition) {
                        return null;
                    }
                });
    }


    private void initializePlot2() {

        // Initialize PieChart reference
        PieChart plot2 = (PieChart) view.findViewById(R.id.plotDistr);

        // Clear plot
        plot2.clear();

        // Create formatter for pie plot
        SegmentFormatter sf1 = new SegmentFormatter(R.color.green);

        // Calculate Percentages
        String[] perc = new String[stringJobs.size()];
        double sum = 0;
        for(int i : stringJobs){
            sum += (double) i;
        }
        for(int j = 1; j < stringJobs.size() - 1; j++) {
            perc[j] = Integer.toString( (int) Math.round((double)stringJobs.get(j) / sum * 100) );
        }

        // Check if there is any data
        boolean noPieData = (stringJobs.equals(new ArrayList<Integer>(Collections.nCopies(stringJobs.size(),0))));

        // Setup segments and add segment to plot ( Data is in stringJobs arraylist)
        Segment s;
        for (int k = 1; k < stringJobs.size() - 1; k++) {

            s = new Segment("#"+Integer.toString(racketNumbers.get(k))+" ("+perc[k]+"%)", stringJobs.get(k));
            plot2.addSeries(s, sf1);
        }

        // Enter dummy data if no stringjobs exist
        if (noPieData) {
            s = new Segment(":-)",1);
            plot2.addSeries(s, sf1);
        }

        // CUSTOMIZE Plot (Did not work through xml)
        plot2.getRenderer(PieRenderer.class).setStartDegs(0);
        plot2.getRenderer(PieRenderer.class).setDonutSize(0.5f, PieRenderer.DonutMode.PERCENT);

        // CUSTOMIZE Formatter (Did not work through xml)
        sf1.getFillPaint().setColor(ContextCompat.getColor(requireContext(), R.color.green));
        sf1.getLabelPaint().setTextSize(getResources().getDimension(R.dimen.pie_segment_label_font_size));
        sf1.getRadialEdgePaint().setColor(ContextCompat.getColor(requireContext(), R.color.grey));
        sf1.getOuterEdgePaint().setColor(ContextCompat.getColor(requireContext(), R.color.grey));
        sf1.getInnerEdgePaint().setColor(ContextCompat.getColor(requireContext(), R.color.grey));

        // Redraw plot
        plot2.redraw();
    }

    private void initializePlot3() {

        // Initialize XYPlot reference
        XYPlot plot3 = (XYPlot) view.findViewById(R.id.plotTime);

        //Clear plot
        plot3.clear();

        // Check if there is any data
        boolean noTimeData = stringJobsSixMonth.size()==1 && stringJobsSixMonth.get(0)==0;

        // Turn above arrayList into XYSeries
        XYSeries timeSeries = new SimpleXYSeries(
                stringJobsSixMonth,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                "Time Series"
        );

        // Create a formatter to use for drawing a series using LineAndPointRenderer
        // and configure it from xml
        LineAndPointFormatter lpf = new LineAndPointFormatter();
        lpf.setPointLabelFormatter(new PointLabelFormatter(R.color.grey,0f,-15f)); //Color.GRAY

        // Add data series to plot
        if (!noTimeData) {
            plot3.addSeries(timeSeries, lpf);
        }
        plot3.redraw();

        // Plot Margins and Padding
        plot3.setPlotMargins(10,0,10,30);
        plot3.setPlotPadding(20,10,20,10);

        // Graph Margins and Padding
        plot3.getGraph().setMargins(0,0,0,0);
        plot3.getGraph().setPadding(0,0,0,125);

        // Determine and set ticks interval on y-axis (Adapts to max number of string jobs.)
        plot3.setRangeStep(StepMode.INCREMENT_BY_VAL, Math.floor(((double) Collections.max(stringJobsSixMonth)) / 6d) + 1);

        // CUSTOMIZE (which was not possible through xml)

        // Set boundaries of y-range
        plot3.setRangeLowerBoundary(0,BoundaryMode.FIXED);
        plot3.setRangeUpperBoundary(Collections.max(stringJobsSixMonth)*1.10,BoundaryMode.FIXED);

        // Fix y-range if all entries are zero
        boolean allZero = false;
        for (int sM : stringJobsSixMonth) {
            if (sM == 0){
                allZero = true;
            }
            else {
                allZero = false;
                break;
            }
        }

        if (allZero) {
            plot3.setRangeBoundaries(0,0.5, BoundaryMode.FIXED);
        }

        // Colors
        lpf.getFillPaint().setColor(Color.TRANSPARENT);
        lpf.getLinePaint().setColor(ContextCompat.getColor(requireContext(), R.color.green));
        lpf.getVertexPaint().setColor(ContextCompat.getColor(requireContext(), R.color.green));
        lpf.getLinePaint().setStrokeWidth(5);
        lpf.getVertexPaint().setStrokeWidth(20);


        // Remove stuff
        plot3.getLayoutManager().remove(plot3.getTitle());

        // Configure custom number Format !! (Convert integer to names of month)
        plot3.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).
                setFormat(new NumberFormat() {
            @Override
            public StringBuffer format(double value, StringBuffer buffer, FieldPosition field) {

                // Current month
                SimpleDateFormat sdf = new SimpleDateFormat("M");
                String cMonth = sdf.format(new java.util.Date());
                int cMonthInt = Integer.parseInt(cMonth);
                int month = (int) ((value + cMonthInt + 12 - 6) % 12);

                return new StringBuffer(DateFormatSymbols.getInstance().getShortMonths()[month] );
            }

            @Override
            public StringBuffer format(long value, StringBuffer buffer, FieldPosition field) {
                //throw new UnsupportedOperationException("Not yet implemented.");
                return null;
            }

            @Override
            public Number parse(String string, ParsePosition position) {
                //throw new UnsupportedOperationException("Not yet implemented.");
                return null;
            }
        });


    }
    /**
     * Static update method
     */
//    public static void updatePlots() {}

    public static void updatePlots() {

        // Get Data ********************************************************************************

        // Create database handler
        RacketDbHelper dbHelper = new RacketDbHelper(view.getContext());

        // Get Stringjobs List
        List<Integer> stringJobs = dbHelper.getAllStringJobsSpinner(itemListID);

        // Get "Last six month"
        List<Integer> stringJobsSixMonth = dbHelper.getMonthlyStringJobsSpinnerSixMonth(itemListID);

        // Get number of all rackets of a specific model and owner
        final List<Integer> racketNumbers = dbHelper.getRacketNumbersNameOwner(itemListID);


        // PLOT 1 ***************************************************************************

        // Initialize XYPlot reference
        XYPlot plot1 = (XYPlot) view.findViewById(R.id.plotCount);

        // Is there any data?
        boolean noData = (stringJobs.equals(new ArrayList<Integer>(Collections.nCopies(stringJobs.size(),0))));

        // // Modify entry of zero stringjob rackets
        List<Float> plot1Data = new ArrayList<Float>();
        Float tmp;
        for (Integer i = 0; i < stringJobs.size(); i++) {

            tmp = (float) stringJobs.get(i);
            if (tmp == 0.0 && i!=0 && i != stringJobs.size() -1) {
                tmp = (float) 0.05;
            }
            plot1Data.add(tmp);
        }

        // Turn above array into XYSeries
        // (No conversion needed, since stringJobs is already an ArrayList)
        XYSeries stringJobsSeries = new SimpleXYSeries(
                plot1Data,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                "Stringjob Series"
        );

        // Create a formatter for bar plot
        BarFormatter bf1 = new BarFormatter(ContextCompat.getColor(view.getContext(), R.color.green), Color.TRANSPARENT);

        // Add label (numbers) to bars
        bf1.setPointLabelFormatter(new PointLabelFormatter(Color.WHITE, 0f, 50f));
        bf1.setPointLabeler(new PointLabeler() {
            @Override
            public String getLabel(XYSeries series, int index) {
                int yValueRounded = Math.round((float) series.getY(index));
                if (yValueRounded == 0) {
                    return "";
                } else {
                    return String.valueOf(Math.round((float) series.getY(index)));
                }
            }
        });

        // Replace data series
        plot1.clear();
        plot1.addSeries(stringJobsSeries, bf1);
        plot1.redraw();

        // Determine and set ticks interval on y-axis (Adapts to max number of string jobs.)
        plot1.setRangeStep(StepMode.INCREMENT_BY_VAL,Math.floor(((double)Collections.max(stringJobs))/6d)+1);

        // Fix ranges of y-axis if plot is empty
        if(!noData) {
            plot1.setRangeBoundaries(0, 0, BoundaryMode.AUTO); // Auto: values don't apply
        } else {
            plot1.setRangeBoundaries(0,0.5, BoundaryMode.FIXED);
        }

        // Setup Bars
        if (!noData) { // only works with data in plot
            BarRenderer barRenderer = plot1.getRenderer(BarRenderer.class);
            barRenderer.setBarOrientation(BarRenderer.BarOrientation.SIDE_BY_SIDE);
            barRenderer.setBarGroupWidth(BarRenderer.BarGroupWidthMode.FIXED_GAP, PixelUtils.dpToPix(20));
        }


        final int stringJobsSize = stringJobs.size();

        // Custom number Format !! (delete 0 and max)
        plot1.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).
                setFormat(new NumberFormat() {
                    @Override
                    public StringBuffer format(double value, StringBuffer buffer, FieldPosition field) {
                        if (value == 0 || value == stringJobsSize-1){
                            return new StringBuffer("");
                        }
                        else {
                            //plot1.setRangeValueFormat(new DecimalFormat("#"));
                            DecimalFormat nf = new DecimalFormat("#");
                            return new StringBuffer('#'+nf.format(racketNumbers.get((int) value)));
                        }
                    }

                    @Override
                    public StringBuffer format(long l, StringBuffer stringBuffer, FieldPosition fieldPosition) {
                        return null;
                    }

                    @Override
                    public Number parse(String s, ParsePosition parsePosition) {
                        return null;
                    }

                });



        // PLOT 2 ***************************************************************************

        // Initialize PieChart reference
        PieChart plot2 = (PieChart) view.findViewById(R.id.plotDistr);

        // Clear plot
        plot2.clear();

        // Create formatter for pie plot
        SegmentFormatter sf1 = new SegmentFormatter(R.color.green);

        // Calculate Percentages
        String[] perc = new String[stringJobs.size()];
        double sum = 0;
        for(int i : stringJobs){
            sum += (double) i;
        }
        for(int j = 1; j < stringJobs.size() - 1; j++) {
            perc[j] = Integer.toString( (int) Math.round((double)stringJobs.get(j) / sum * 100) );
        }

        // Check if there is any data
        boolean noPieData = (stringJobs.equals(new ArrayList<Integer>(Collections.nCopies(stringJobs.size(),0))));

        // Setup segments and add segments to plot ( Data is in stringJobs arraylist)
        Segment s;
        for (int k = 1; k < stringJobs.size() - 1; k++) {

            s = new Segment("#"+Integer.toString(racketNumbers.get(k))+" ("+perc[k]+"%)", stringJobs.get(k));
            plot2.addSeries(s, sf1);
        }

        // Enter dummy data if no stringjobs exist
        if (noPieData) {
            s = new Segment("",1);
            plot2.addSeries(s, sf1);
        }

        // CUSTOMIZE Formatter (Did not work through xml)
        sf1.getFillPaint().setColor(ContextCompat.getColor(view.getContext(), R.color.green));
        sf1.getLabelPaint().setTextSize(view.getResources().getDimension(R.dimen.pie_segment_label_font_size));
        sf1.getRadialEdgePaint().setColor(ContextCompat.getColor(view.getContext(), R.color.grey));
        sf1.getOuterEdgePaint().setColor(ContextCompat.getColor(view.getContext(), R.color.grey));
        sf1.getInnerEdgePaint().setColor(ContextCompat.getColor(view.getContext(), R.color.grey));

        // Redraw plot
        plot2.redraw();


        // PLOT 3 ***************************************************************************

        // Initialize XYPlot reference
        XYPlot plot3 = (XYPlot) view.findViewById(R.id.plotTime);

        // Clear plot
        plot3.clear();

        // Check if there is any data
        boolean noTimeData = stringJobsSixMonth.size()==1 && stringJobsSixMonth.get(0)==0;

        // Turn above arrayList into XYSeries
        XYSeries timeSeries = new SimpleXYSeries(
                stringJobsSixMonth,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                "Time Series"
        );

        // Create a formatter to use for drawing a series using LineAndPointRenderer
        // and configure it from xml
        LineAndPointFormatter lpf = new LineAndPointFormatter();
        lpf.setPointLabelFormatter(new PointLabelFormatter(R.color.grey,0f,-15f)); //Color.GRAY


        // add a new series' to the xyplot:
        //plot1.addSeries(series2, bf1);
        if (!noTimeData) {
            plot3.addSeries(timeSeries, lpf);
        }

        // CUSTOMIZE (which was not possible through xml)

        // y-range and ticks
        plot3.setRangeStep(StepMode.INCREMENT_BY_VAL, Math.floor(((double) Collections.max(stringJobsSixMonth)) / 6d) + 1);
        plot3.setRangeUpperBoundary(Collections.max(stringJobsSixMonth)*1.10,BoundaryMode.FIXED);

        // Fix y-range if all entries are zero
        boolean allZero = false;
        for (int sM : stringJobsSixMonth) {
            if (sM == 0){
                allZero = true;
            }
            else {
                allZero = false;
                break;
            }
        }

        if (allZero) {
            plot3.setRangeBoundaries(0,0.5, BoundaryMode.FIXED);
        }

        // Colors
        lpf.getFillPaint().setColor(Color.TRANSPARENT);
        lpf.getLinePaint().setColor(ContextCompat.getColor(view.getContext(), R.color.green));
        lpf.getVertexPaint().setColor(ContextCompat.getColor(view.getContext(), R.color.green));
        lpf.getLinePaint().setStrokeWidth(5);
        lpf.getVertexPaint().setStrokeWidth(20);

        // Remove stuff
        plot3.getLayoutManager().remove(plot3.getTitle());

        // Redraw plot
        plot3.redraw();

    }



}
