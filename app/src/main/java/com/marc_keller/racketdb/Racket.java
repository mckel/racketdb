/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;

/**
 * Created by mck on 24.09.14.
 */
public class Racket {

    // Fields
    private int id;
    private String owner;
    private String model;
    private int number;
    private String defaultTension;
    private String defaultString;
    private double defaultDiameter;
    private String defaultCrossString;
    private double defaultCrossDiameter;

    // Constructors
    public Racket(String owner, String model, int number, String defaultTension, String defaultString, double defaultDiameter, String defaultCrossString, double defaultCrossDiameter) {
        this.owner = owner;
        this.model = model;
        this.number = number;
        this.defaultTension = defaultTension;
        this.defaultString = defaultString;
        this.defaultDiameter = defaultDiameter;
        this.defaultCrossString = defaultCrossString;
        this.defaultCrossDiameter = defaultCrossDiameter;
    }

    public Racket(String owner, String model, int number, String defaultTension, String defaultString, double defaultDiameter) {
        this.owner = owner;
        this.model = model;
        this.number = number;
        this.defaultTension = defaultTension;
        this.defaultString = defaultString;
        this.defaultDiameter = defaultDiameter;
        this.defaultCrossString = "";
        this.defaultCrossDiameter = Double.NaN;
    }

    public Racket(String owner, String model, int number) {
        this.owner = owner;
        this.model = model;
        this.number = number;
        this.defaultTension = "";
        this.defaultString = "";
        this.defaultDiameter = Double.NaN;
        this.defaultCrossString = "";
        this.defaultCrossDiameter = Double.NaN;
    }

    public Racket(int id, String owner, String model, int number, String defaultTension, String defaultString, double defaultDiameter,String defaultCrossString, double defaultCrossDiameter) {
        this.id = id;
        this.owner = owner;
        this.model = model;
        this.number = number;
        this.defaultTension = defaultTension;
        this.defaultString = defaultString;
        this.defaultDiameter = defaultDiameter;
        this.defaultCrossString = defaultCrossString;
        this.defaultCrossDiameter = defaultCrossDiameter;
    }

    public Racket(int id, String owner, String model, int number, String defaultTension, String defaultString, double defaultDiameter) {
        this.id = id;
        this.owner = owner;
        this.model = model;
        this.number = number;
        this.defaultTension = defaultTension;
        this.defaultString = defaultString;
        this.defaultDiameter = defaultDiameter;
        this.defaultCrossString = "";
        this.defaultCrossDiameter = Double.NaN;
    }

    // Getter and Setter
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getOwner() {
        return owner;
    }
    public void setOwner(String owner) {
        this.owner = owner;
    }
    public String getModel() { return model; }
    public void setModel(String model) {
        this.model = model;
    }
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getDefaultTension() { return defaultTension; }
    public String getDefaultTensionNumbers() {
        String[] tension_parts = defaultTension.split("/");
        String tension_numbers;
        if (tension_parts.length < 2) {
            tension_numbers = defaultTension;
        } else if (tension_parts.length < 3) {
            tension_numbers = String.join("/", tension_parts[0], tension_parts[1]);
        } else {
            tension_numbers = String.join("/", tension_parts[1], tension_parts[2]);
        }
        return tension_numbers;
    }
    public String getDefaultTensionUnit() {
        String[] tension_parts = defaultTension.split("/");
        String unit;
        if (tension_parts.length == 3) {
            if (tension_parts[0].equals("lbs")) {
                unit = "lbs";
            } else {
                unit = "kg";
            }
        } else {
            unit = "kg";
        }
        return unit;
    }
    public void setDefaultTension(String defaultTension) { this.defaultTension = defaultTension; }
    public String getDefaultString() { return defaultString; }
    public void setDefaultString(String defaultString) { this.defaultString = defaultString; }
    public double getDefaultDiameter() { return defaultDiameter; }
    public void setDefaultDiameter(double defaultDiameter) { this.defaultDiameter = defaultDiameter; }
    public String getDefaultCrossString() { return defaultCrossString; }
    public void setDefaultCrossString(String defaultString) { this.defaultCrossString = defaultString; }
    public double getDefaultCrossDiameter() { return defaultCrossDiameter; }
    public void setDefaultCrossDiameter(double defaultDiameter) { this.defaultCrossDiameter = defaultDiameter; }
}