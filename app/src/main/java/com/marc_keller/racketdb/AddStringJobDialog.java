/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;


import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Arrays;
import java.util.Calendar;


public class AddStringJobDialog {

    static private int idDB;
    static private StringJob stringJob;


    public static class SelectDateDialog extends AppCompatDialogFragment implements DatePickerDialog.OnDateSetListener {

        /**
         * Create a new instance of MyDialogFragment, providing "id"
         * as an argument.
         */
        static SelectDateDialog newInstance(int id) {
            SelectDateDialog d = new SelectDateDialog();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("racketID", id);
            d.setArguments(args);

            return d;
        }


        @Override
        public DatePickerDialog onCreateDialog(Bundle paramBundle) {
            super.onCreateDialog(paramBundle);
            // Get argument
            idDB = getArguments().getInt("racketID");


            // Use the Builder class for convenient dialog construction
            //AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            //builder.setTitle(R.string.dialogtitle_selectdate);

            // Use the current date as the default date in the picker
            Calendar localC = Calendar.getInstance();
            int y = localC.get(Calendar.YEAR);
            int m = localC.get(Calendar.MONTH);
            int d = localC.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dp = new DatePickerDialog(getActivity(),SelectDateDialog.this,y,m,d);
            dp.setTitle(R.string.dialogtitle_datepicker);

            return dp;

            //return new DatePickerDialog(getActivity(),SelectDateDialog.this, y, m, d);
        }


        boolean fired = false;

        // Finished Button
        @Override
        public void onDateSet(DatePicker paramDatePicker, int year, int month, int day) {
            // This workaround is needed because there is a bug that runs the onDateSet method of
            // the DatePicker Dialog method twice!
            if (fired) {
                return;
            } else {
                fired = true;
            }

            // set id to temp stringjob
            stringJob = new StringJob();
            stringJob.setRacketID(idDB);

            // Set date to temp stringjob
            month++;
            // stringJob.setDate(day + "." + month + "." + year);
            String monthStr = String.valueOf(month);
            if(month<10) {
                monthStr = "0" + monthStr;
            }
            String dayStr = String.valueOf(day);
            if(day<10) {
                dayStr = "0" + dayStr;
            }
            stringJob.setDate(year + "-" + monthStr + "-" + dayStr + " 11:11:11.111"); // SQLite format

            // Call next dialog (Tension picker)
            FragmentManager fmManager = getActivity().getSupportFragmentManager();
            SelectTensionDialog d = new SelectTensionDialog();
            d.show(fmManager, "TensionPicker");
        }
    }

    public static class SelectTensionDialog extends AppCompatDialogFragment {
        View view;
        String[] dispEntriesKG;
        String[] dispEntriesLBS;
        SwitchCompat tensionSwitch;
        NumberPicker npMains;
        NumberPicker npCross;

        @Override
        public AppCompatDialog onCreateDialog(Bundle paramBundle) {
            // Use Builder to customize dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            //Inflate custom layout and bind to builder
            view = getActivity().getLayoutInflater().inflate(R.layout.selecttension_dialog, null);
            builder.setView(view);

            // Dialog title
            builder.setTitle(R.string.dialogtitle_tension);

            // Get Numberpickers from view
            npMains = (NumberPicker) view.findViewById(R.id.npMains);
            npCross = (NumberPicker) view.findViewById(R.id.npCross);

            // Attach Listener to kg/lbs Switch
            tensionSwitch = (SwitchCompat) view.findViewById(R.id.switchTensionDimension);
            tensionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        // (Switched from kg to lbs)

                        // Get current entries
                        double mainKG = Double.parseDouble(dispEntriesKG[npMains.getValue()]);
                        double crossKG = Double.parseDouble(dispEntriesKG[npCross.getValue()]);

                        // Set value to 0 (otherwise index might exceed new length of array)
                        npMains.setValue(0);
                        npCross.setValue(0);

                        // lbs-specific things
                        // for mains
                        npMains.setMinValue(0);
                        npMains.setDisplayedValues(dispEntriesLBS);
                        npMains.setMaxValue(dispEntriesLBS.length-1);

                        // for crosses
                        npCross.setMinValue(0);
                        npCross.setDisplayedValues(dispEntriesLBS);
                        npCross.setMaxValue(dispEntriesLBS.length-1);

                        // Calculate corresponding lbs Values
                        double mainLBS = Math.round(10.0 * mainKG * 2.20462262) / 10.0;
                        double crossLBS = Math.round(10.0 * crossKG * 2.20462262) / 10.0;

                        // Set corresponding lbs value
                        npMains.setValue(Arrays.asList(dispEntriesLBS).indexOf(Double.toString(mainLBS)));
                        npCross.setValue(Arrays.asList(dispEntriesLBS).indexOf(Double.toString(crossLBS)));


                    } else {
                        // (Switched from lbs to kg)

                        // Get current entries
                        double mainLBS = Double.parseDouble(dispEntriesLBS[npMains.getValue()]);
                        double crossLBS = Double.parseDouble(dispEntriesLBS[npCross.getValue()]);

                        // Set value to 0 (otherwise index might exceed new length of array)
                        npMains.setValue(0);
                        npCross.setValue(0);

                        // kg-specific things
                        //(mains)
                        npMains.setMinValue(0);
                        npMains.setMaxValue(dispEntriesKG.length-1);
                        npMains.setDisplayedValues(dispEntriesKG);

                        //(crosses)
                        npCross.setMinValue(0);
                        npCross.setMaxValue(dispEntriesKG.length-1);
                        npCross.setDisplayedValues(dispEntriesKG);

                        // Calculate corresponding kg Values
                        double mainKG = Math.round(10.0 * mainLBS *  0.45359237) / 10.0;
                        double crossKG = Math.round(10.0 * crossLBS *  0.45359237) / 10.0;

                        // Set corresponding kg value
                        npMains.setValue(Arrays.asList(dispEntriesKG).indexOf(Double.toString(mainKG)));
                        npCross.setValue(Arrays.asList(dispEntriesKG).indexOf(Double.toString(crossKG)));
                    }
                }
            });

            // Customize Number Picker
            // mains
            npMains.setFocusableInTouchMode(false);
            npMains.setMinValue(0);
            npMains.setWrapSelectorWheel(false);

            // crosses
            npCross.setFocusableInTouchMode(false);
            npCross.setMinValue(0);
            npCross.setWrapSelectorWheel(false);

            // Define displayed entries
            // min: 5kg --> 11lbs
            // max: 45kg --> 99lbs

            // kg entries
            //int nEntriesKG = 80 + 1;
            double kg_min = 5.0;
            double kg_max = 45.0;
            double dx = 0.1;
            int nEntriesKG = (int) ((kg_max-kg_min)/dx + 1);
            dispEntriesKG = new String[nEntriesKG];
            for (int k = 0; k < dispEntriesKG.length; k++) {
                dispEntriesKG[k] = Double.toString(Math.round(10.0 * (kg_min + k * dx)) / 10.0);
            }

            // lbs entries
            //int nEntriesLBS = 176 + 1;
            double lbs_min = 11.0;
            double lbs_max = 99.0;
            int nEntriesLBS = (int) ((lbs_max-lbs_min)/dx + 1);
            dispEntriesLBS = new String[nEntriesLBS];
            for (int k = 0; k < dispEntriesLBS.length; k++) {
                dispEntriesLBS[k] = Double.toString(Math.round(10.0 * (lbs_min + k * dx)) / 10.0);
            }

            // Set default values (in kg)
            tensionSwitch.setChecked(false);
            // for mains
            npMains.setDisplayedValues(dispEntriesKG);
            npMains.setMaxValue(nEntriesKG - 1);
            npMains.setValue(200); // = 25.0
            // for crosses
            npCross.setDisplayedValues(dispEntriesKG);
            npCross.setMaxValue(nEntriesKG - 1);
            npCross.setValue(190); // = 24.0

            // Overwrite with custom default values
            RacketDbHelper dbH = new RacketDbHelper(getActivity());
            Racket r = dbH.getRacket(idDB);
            if (r.getDefaultTension() != null && !r.getDefaultTension().isEmpty()) {
                String unit = r.getDefaultTensionUnit();
                String tension = r.getDefaultTensionNumbers();
                String[] tension_parts = tension.split("/");
                double mains = Double.parseDouble(tension_parts[0]);
                double crosses = Double.parseDouble(tension_parts[1]);
                if (unit.equals("lbs")) {
                    tensionSwitch.setChecked(true);
                    npMains.setValue(Arrays.asList(dispEntriesLBS).indexOf(Double.toString(mains)));
                    npCross.setValue(Arrays.asList(dispEntriesLBS).indexOf(Double.toString(crosses)));
                } else {
                    tensionSwitch.setChecked(false);
                    npMains.setValue(Arrays.asList(dispEntriesKG).indexOf(Double.toString(mains)));
                    npCross.setValue(Arrays.asList(dispEntriesKG).indexOf(Double.toString(crosses)));
                }
            }
            dbH.close();

            // Next Button
            DialogInterface.OnClickListener listenerPositive = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInt, int paramInt) {
                    String[] usedValues = npMains.getDisplayedValues();
                    double mains = Double.parseDouble(usedValues[npMains.getValue()]);
                    double cross = Double.parseDouble(usedValues[npCross.getValue()]);
                    String unit = (tensionSwitch.isChecked()) ? "lbs" : "kg";

                    // Set tension to temp stringjob
                    stringJob.setTension(String.join("/",unit,Double.toString(mains),Double.toString(cross)));

                    // Call next dialog (string data)
                    FragmentManager fmManager = getActivity().getSupportFragmentManager();
                    StringDataDialog d = new StringDataDialog();
                    d.show(fmManager, "StringDataDialog");

                }
            };
            builder.setPositiveButton(R.string.Next_String, listenerPositive);

            // Cancel Button
            DialogInterface.OnClickListener listenerNegative = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            };
            builder.setNegativeButton(R.string.String_Cancel, listenerNegative);

            // return dialog
            return builder.create();
        }
    }

    public static class StringDataDialog extends AppCompatDialogFragment {

        View view;
        SwitchCompat swCrosses;
        CheckBox cbDefault;
        Button btnReset;

        @Override
        public AppCompatDialog onCreateDialog(Bundle paramBundle) {
            // Use Builder to customize dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            //Inflate custom layout and bind to builder
            view = getActivity().getLayoutInflater().inflate(R.layout.stringdata_dialog, null);
            builder.setView(view);

            // Set default values
            RacketDbHelper dbH = new RacketDbHelper(getActivity());
            Racket r = dbH.getRacket(idDB);

            swCrosses = (SwitchCompat) view.findViewById(R.id.switch_addcrosses);
            cbDefault = (CheckBox) view.findViewById(R.id.cb_stringdefault);
            swCrosses.setChecked(false);
            cbDefault.setChecked(false);

            TextInputLayout cnam_ti = view.findViewById(R.id.eT_crossstringdata_name_ti);
            TextInputLayout cdia_ti = view.findViewById(R.id.eT_crossstringdata_diameter_ti);
            cnam_ti.setVisibility(View.GONE);
            cdia_ti.setVisibility(View.GONE);

            if (r.getDefaultString() != null && !r.getDefaultString().isEmpty() && !Double.isNaN(r.getDefaultDiameter())) {
                //cbDefault.setChecked(true);
                TextInputEditText nam = view.findViewById(R.id.eT_stringdata_name);
                TextInputEditText dia = view.findViewById(R.id.eT_stringdata_diameter);
                nam.setText(r.getDefaultString());
                dia.setText(Double.toString(r.getDefaultDiameter()));
            }

            if (r.getDefaultCrossString() != null && !r.getDefaultCrossString().isEmpty() && !Double.isNaN(r.getDefaultCrossDiameter())) {
                swCrosses.setChecked(true);
                cnam_ti.setVisibility(View.VISIBLE);
                cdia_ti.setVisibility(View.VISIBLE);
                TextInputEditText cnam = view.findViewById(R.id.eT_crossstringdata_name);
                TextInputEditText cdia = view.findViewById(R.id.eT_crossstringdata_diameter);
                cnam.setText(r.getDefaultCrossString());
                cdia.setText(Double.toString(r.getDefaultCrossDiameter()));
            }
            dbH.close();

            // Reset defaults button
            btnReset = (Button) view.findViewById(R.id.button_reset);
            btnReset.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    RacketDbHelper dbH = new RacketDbHelper(getActivity());
                    Racket r = dbH.getRacket(idDB);

                    r.setDefaultTension("");
                    r.setDefaultString("");
                    r.setDefaultDiameter(Double.NaN);
                    r.setDefaultCrossString("");
                    r.setDefaultCrossDiameter(Double.NaN);

                    dbH.updateRacket(r);
                    dbH.close();

                    TextInputEditText nam = view.findViewById(R.id.eT_stringdata_name);
                    TextInputEditText dia = view.findViewById(R.id.eT_stringdata_diameter);
                    nam.setText("");
                    dia.setText("");

                    TextInputEditText cnam = view.findViewById(R.id.eT_crossstringdata_name);
                    TextInputEditText cdia = view.findViewById(R.id.eT_crossstringdata_diameter);
                    cnam.setText("");
                    cdia.setText("");

                    Toast.makeText(getActivity(), getResources().getString(R.string.resetdefaultstoast), Toast.LENGTH_SHORT).show();
                }
            });


            // Dialog title
            builder.setTitle(R.string.dialogtitle_stringdata);

            // Show separate fields for crosses (if checked)
            swCrosses.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    TextInputLayout cnam_ti = view.findViewById(R.id.eT_crossstringdata_name_ti);
                    TextInputLayout cdia_ti= view.findViewById(R.id.eT_crossstringdata_diameter_ti);

                    if (isChecked) {
                        // Show extra fields
                        cnam_ti.setVisibility(View.VISIBLE);
                        cdia_ti.setVisibility(View.VISIBLE);
                    } else {
                        // Hide extra fields
                        cnam_ti.setVisibility(View.GONE);
                        cdia_ti.setVisibility(View.GONE);
                    }
                }
            });

            // Cancel Button
            DialogInterface.OnClickListener listenerNegative = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            };
            builder.setNegativeButton(R.string.String_Cancel, listenerNegative);

            // Next Button (created by AlertDialog)
            //--> Custom Listener needed to check the input!
            builder.setPositiveButton(R.string.finished, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // All of the fun happens inside the CustomListener now!
                    // I had to move it to enable data validation.
                }
            });

            // Create dialog with now created settings
            AlertDialog addDialog = builder.create();
            // show dialog
            addDialog.show();

            // Assign (custom) Listener to Positive Button
            Button addButton = addDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            addButton.setOnClickListener(new CustomStringDataListener(addDialog));

            // return dialog
            return addDialog;
        }


        /**
         * ******************************************************************************************
         * Custom Listener
         */

        // Custom Listener to check filled out text fields.
        public class CustomStringDataListener implements View.OnClickListener {

            private final AppCompatDialog dialog;

            // Formular Entries
            private TextInputEditText string;
            private TextInputEditText diameter;
            private TextInputEditText stringCrosses;
            private TextInputEditText diameterCrosses;
            private TextInputEditText note;

            // checkbox/switch
            private CheckBox cbDefault;
            private SwitchCompat swCrosses;

            public CustomStringDataListener(AlertDialog dialog) {
                this.dialog = dialog;
                this.string = dialog.findViewById(R.id.eT_stringdata_name);
                this.diameter = dialog.findViewById(R.id.eT_stringdata_diameter);
                this.stringCrosses = dialog.findViewById(R.id.eT_crossstringdata_name);
                this.diameterCrosses = dialog.findViewById(R.id.eT_crossstringdata_diameter);
                this.cbDefault = dialog.findViewById(R.id.cb_stringdefault);
                this.swCrosses = dialog.findViewById(R.id.switch_addcrosses);
		        this.note = dialog.findViewById(R.id.eT_stringdata_note);
            }

            @Override
            public void onClick(View v) {
                // CHECK if all required text fields are filled out:
                // if Yes: Save data and Open next dialog
                // if No: Inform user and keep dialog open
                if (checkInput()) {

                    // Check if strings contain special characters
                    if (!MainActivity.checkString(string.getText().toString().trim()) ||
                            !MainActivity.checkString(stringCrosses.getText().toString().trim())) {
                        // if Yes: Inform user and keep dialog open
                        Toast.makeText(getActivity(), getResources().getString(R.string.stringSpecial), Toast.LENGTH_SHORT).show();
                    } else {
                        // Set string data to temp stringjob
                        stringJob.setString(string.getText().toString().trim());
                        stringJob.setDiameter(Double.parseDouble(diameter.getText().toString().trim()));
                        stringJob.setNote(note.getText().toString());

                        if (swCrosses.isChecked()){
                            stringJob.setStringCrosses(stringCrosses.getText().toString().trim());
                            stringJob.setDiameterCrosses(Double.parseDouble(diameterCrosses.getText().toString().trim()));
                        }

                        // Add to Database
                        RacketDbHelper dbHelper = new RacketDbHelper(getActivity());
                        dbHelper.addStringjob(stringJob);

                        // Assign Default values (to corresponding racket)
                        Racket r = dbHelper.getRacket(idDB);
                        if (cbDefault.isChecked()) {
                            r.setDefaultTension((stringJob.getTension().trim()));
                            r.setDefaultString(string.getText().toString().trim());
                            r.setDefaultDiameter(Double.parseDouble(diameter.getText().toString().trim()));
                        }
                        if (cbDefault.isChecked() && swCrosses.isChecked()) {
                            r.setDefaultCrossString(stringCrosses.getText().toString().trim());
                            r.setDefaultCrossDiameter(Double.parseDouble(diameterCrosses.getText().toString().trim()));
                        }
                        dbHelper.updateRacket(r);

                        // UPDATE LISTVIEW
                        ListView racketLV = (ListView) getActivity().findViewById(R.id.lv_rackets);
                        RacketListAdapter racketListAdapter = (RacketListAdapter) ((HeaderViewListAdapter)racketLV.getAdapter()).getWrappedAdapter();
                        // Get all entries from database and create cursor
                        Cursor newCursor = dbHelper.getAllRacketsAlphabetic();
                        racketListAdapter.changeCursor(newCursor);
                        racketListAdapter.notifyDataSetChanged();
                        // stringjob listview not necessary since fragmentC not shown!

                        // Update Plots
                        FragmentB.updatePlots();

                        // Inform user
                        Toast.makeText(getActivity(), getResources().getString(R.string.stringjobAdded), Toast.LENGTH_SHORT).show();

                        // Exit dialog
                        dialog.dismiss();
                    }
                } else {
                    // No: Inform user and keep dialog open
                    Toast.makeText(getActivity(), getResources().getString(R.string.stringjobFill), Toast.LENGTH_SHORT).show();
                }
            }

            public boolean checkInput(){
                if (!string.getText().toString().trim().isEmpty()
                        && !diameter.getText().toString().trim().isEmpty()) {
                    if (swCrosses.isChecked()) {
                        if (!stringCrosses.getText().toString().trim().isEmpty()
                                && !diameterCrosses.getText().toString().trim().isEmpty()) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }

            }
        }
    }
}

