/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;


import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HeaderViewListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

/**
 * Created by mck on 25.09.14.
 */
public class AddRacketDialog extends AppCompatDialogFragment {


    @Override
    public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Inflate custom layout
        View v= getActivity().getLayoutInflater().inflate(R.layout.addracket_dialog, null);

        // Customize dialog
        builder.setView(v);
        builder.setTitle(R.string.dialogtitle_addracket);


        // Cancel Button (use standard AlertDialog button)
        DialogInterface.OnClickListener listenerNegative = new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
            }
        };
        builder.setNegativeButton(R.string.String_Cancel,listenerNegative);

        //--> Custom Listener needed to check the input!
        builder.setPositiveButton(R.string.String_Add, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // All of the fun happens inside the CustomListener now!
                //I had to move it to enable data validation.
            }
        });

        // Create dialog with now created settings
        AlertDialog addDialog = builder.create();
        // show dialog
        addDialog.show();

        // Assign (custom) Listener to Positive Button
        Button addButton = addDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        addButton.setOnClickListener(new CustomAddRacketListener(addDialog));

        // return dialog
        return addDialog;
    }



    /*********************************************************************************************
     * Custom Listener
     */

    // Custom Listener to check filled out text fields.
    public class CustomAddRacketListener implements View.OnClickListener {
        private final AppCompatDialog dialog;
        // Formular Entries
        private TextInputEditText model;
        private TextInputEditText owner;
        private TextInputEditText number;


        public CustomAddRacketListener(AppCompatDialog dialog) {
            this.dialog = dialog;
            this.model = dialog.findViewById(R.id.eT_addRacket_model);
            this.owner = dialog.findViewById(R.id.eT_addRacket_owner);
            this.number = dialog.findViewById(R.id.eT_addRacket_number);
        }

        @Override
        public void onClick(View v) {
            // CHECK if all required text fields are filled out:
            if(!model.getText().toString().trim().isEmpty()
                    && !owner.getText().toString().trim().isEmpty()) {
                // if Yes: Create new database entry with given racket information and close dialog

                // CHECK if strings contain special characters
                if (!MainActivity.checkString(model.getText().toString().trim())
                        || !MainActivity.checkString(owner.getText().toString().trim())) {
                    // if Yes: Inform user and keep dialog open

                    Toast.makeText(getActivity(), getResources().getString(R.string.stringSpecial), Toast.LENGTH_SHORT).show();

                } else {
                    // if No: Create new database entry with given racket information and close dialog

                    // Set up the dbHelper to manage the database.
                    RacketDbHelper racketDbHelper = new RacketDbHelper(getActivity());

                    // racket number
                    int rNumber;
                    if (number.getText().toString().trim().isEmpty()) {
                        rNumber = 1;
                    } else {
                        rNumber = Integer.parseInt(number.getText().toString());
                    }

                    // Create racket object
                    Racket r = new Racket(
                            owner.getText().toString().trim(),
                            model.getText().toString().trim(),
                            rNumber
                    );

                    // Check if Racket already exists!
                    if (!racketDbHelper.checkRacket(r)) {
                        // Pass racket to database
                        racketDbHelper.addRacket(r);

                        // UPDATE LISTVIEW
                        ListView racketLV = getActivity().findViewById(R.id.lv_rackets);
                        RacketListAdapter racketListAdapter = (RacketListAdapter) ((HeaderViewListAdapter)racketLV.getAdapter()).getWrappedAdapter();
                        // Get all entries from database and create cursor
                        Cursor newCursor = racketDbHelper.getAllRacketsAlphabetic();
                        racketListAdapter.changeCursor(newCursor);
                        racketListAdapter.notifyDataSetChanged();

                        // Spinner Drop down elements
                        Spinner spinner;
                        spinner = getActivity().findViewById(R.id.spinnerHistory);
                        ArrayAdapter<String> dataAdapter = (ArrayAdapter<String>) spinner.getAdapter();
                        List<String> lables = racketDbHelper.getAllRacketsSpinner();
                        dataAdapter.clear();
                        if (lables != null) {
                            for (String str : lables) {
                                dataAdapter.insert(str, dataAdapter.getCount());
                            }
                        }
                        if (dataAdapter.getItem(0).equals(getResources().getString(R.string.pleaseAddRacket))) {
                            spinner.setEnabled(false);
                            spinner.setClickable(false);
                        } else {
                            spinner.setEnabled(true);
                            spinner.setClickable(true);
                        }
                        dataAdapter.notifyDataSetChanged();

                        // Update Plots
                        FragmentB.updatePlots();

                        // Inform user
                        Toast.makeText(getActivity(), getResources().getString(R.string.racketAdded), Toast.LENGTH_SHORT).show();

                        // Exit dialog
                        dialog.dismiss();
                    } else {
                        // No: Inform user and keep dialog open
                        Toast.makeText(getActivity(), getResources().getString(R.string.racketExists), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            else {
                // No: Inform user and keep dialog open
                Toast.makeText(getActivity(), getResources().getString(R.string.racketFill),Toast.LENGTH_SHORT).show();
            }
        }
    }

}