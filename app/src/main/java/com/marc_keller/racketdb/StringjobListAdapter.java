/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

/**
 * Created by mck on 27.09.14.
 */
public class StringjobListAdapter extends CursorAdapter {
    // declare inflater
    private LayoutInflater mInflater;


    // Constructor
    public StringjobListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        // Initialize inflater
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * ******************************************************************************************
     * Override Methods
     */

    // newView method is called to create a View object representing on item in the list,
    // here You just create an object don't set any values
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // LayoutInflater creates a view based on xml structure in resource file
        // passed to inflate method as first parameter.
        return mInflater.inflate(R.layout.listview_stringjobs, parent, false);
    }

    // View returned from newView is passed as first parameter to bindView,
    // it is here where You will set values to display. (bindData)
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        /**
         * This is where the action takes place
         */
        //First You get element to populate by calling view.findViewById(),
        // cast it to correct type,...
        //... and set value.

        //TextView v = (TextView) view.findViewById(R.id.lstV_stringjobs_id);
        //v.setText(cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_RACKETID)));

        TextView v = view.findViewById(R.id.lstV_stringjobs_date);
        v.setText(MainActivity.getDateUI(cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_DATE))));

        v = view.findViewById(R.id.lstV_stringjobs_tension);
        StringJob stringjob_tmp = new StringJob();
        stringjob_tmp.setTension(cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_TENSION)));
        v.setText(String.format("%s %s", stringjob_tmp.getTensionNumbers(),stringjob_tmp.getTensionUnit()));

        v = view.findViewById(R.id.lstV_stringjobs_string);
        v.setText(cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_STRING)));

        v = view.findViewById(R.id.lstV_stringjobs_diam);
        v.setText(String.format("%s mm", cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_DIAMETER))));

        String stringCrosses = cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_STRING_CROSS));
        if (stringCrosses != null && !stringCrosses.isEmpty()) {
            v = view.findViewById(R.id.lstV_stringjobs_mains);
            v.setVisibility(View.VISIBLE);
            v = view.findViewById(R.id.lstV_stringjobs_crosses);
            v.setVisibility(View.VISIBLE);
            v = view.findViewById(R.id.lstV_stringjobs_crossesstringT);
            v.setVisibility(View.VISIBLE);
            v = view.findViewById(R.id.lstV_stringjobs_crossesstring);
            v.setVisibility(View.VISIBLE);
            v.setText(stringCrosses);
            v = view.findViewById(R.id.lstV_stringjobs_crossesdiamT);
            v.setVisibility(View.VISIBLE);
            v = view.findViewById(R.id.lstV_stringjobs_crossesdiam);
            v.setVisibility(View.VISIBLE);
            v.setText(String.format("%s mm", cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_DIAMETER_CROSS))));
        } else {
            v = view.findViewById(R.id.lstV_stringjobs_mains);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.lstV_stringjobs_crosses);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.lstV_stringjobs_crossesstringT);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.lstV_stringjobs_crossesstring);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.lstV_stringjobs_crossesdiamT);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.lstV_stringjobs_crossesdiam);
            v.setVisibility(View.GONE);
        }

        String noteContent = cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_NOTE));
        if (noteContent != null && !noteContent.isEmpty()) {
            v = view.findViewById(R.id.lstV_stringjobs_note);
            v.setVisibility(View.VISIBLE);
            v.setText(noteContent);
            v = view.findViewById(R.id.lstV_stringjobs_noteT);
            v.setVisibility(View.VISIBLE);
        } else {
            v = view.findViewById(R.id.lstV_stringjobs_note);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.lstV_stringjobs_noteT);
            v.setVisibility(View.GONE);
        }


        // Also data from racket db needed
        RacketDbHelper dbHelper = new RacketDbHelper(context);
        Racket r = dbHelper.getRacket(cursor.getInt(cursor.getColumnIndex(RacketDbHelper.KEY_RACKETID)));

        v = view.findViewById(R.id.lstV_stringjobs_model);
        v.setText(r.getModel());

        v = view.findViewById(R.id.lstV_stringjobs_owner);
        v.setText(r.getOwner());

        v = view.findViewById(R.id.lstV_stringjobs_number);
        v.setText(Integer.toString(r.getNumber()));

        dbHelper.close();


        // Space for some fancy customization
        //alternating background color
        if(cursor.getPosition()%2==1) {
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.grey));
        }
        else {
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.grey_darker));
        }
    }
}
