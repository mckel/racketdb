/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;


/**
 * Created by mck on 25.09.14.
 */
public class StringJob {

    // Fields
    private int id; // given by DB
    private int racketID;
    private String date;
    private String tension;
    private String string;
    private double diameter;
    private String stringCrosses;
    private double diameterCrosses;
    private String note;


    // Constructors:
    public StringJob(){
    }
    //all data
    public StringJob(int id, int racketID, String date, String tension, String string, double diameter, String stringCrosses, double diameterCrosses, String note) {
        this.id = id;
        this.racketID = racketID;
        this.date = date;
        this.tension = tension;
        this.string = string;
        this.diameter = diameter;
        this.stringCrosses = stringCrosses;
        this.diameterCrosses = diameterCrosses;
        this.note = note;
    }
    //all data (no id)
    public StringJob(int racketID, String date, String tension, String string, double diameter, String stringCrosses, double diameterCrosses, String note) {
        this.racketID = racketID;
        this.date = date;
        this.tension = tension;
        this.string = string;
        this.diameter = diameter;
        this.stringCrosses = stringCrosses;
        this.diameterCrosses = diameterCrosses;
        this.note = note;
    }

    //all data (no crosses)
    public StringJob(int id, int racketID, String date, String tension, String string, double diameter, String note) {
        this.id = id;
        this.racketID = racketID;
        this.date = date;
        this.tension = tension;
        this.string = string;
        this.diameter = diameter;
        this.stringCrosses = "";
        this.diameterCrosses = Double.NaN;
        this.note = note;
    }

    //all data (no id, no crosses)
    public StringJob(int racketID, String date, String tension, String string, double diameter, String note) {
        this.racketID = racketID;
        this.date = date;
        this.tension = tension;
        this.string = string;
        this.diameter = diameter;
        this.stringCrosses = "";
        this.diameterCrosses = Double.NaN;
        this.note = note;
    }

    // Getter and Setter
    public int getId() { return id; }
    public void setId(int id) {
        this.id = id;
    }
    public int getRacketID() {
        return racketID;
    }
    public void setRacketID(int racketID) {
        this.racketID = racketID;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getTension() {
        return tension;
    }
    public String getTensionNumbers() {
        String[] tension_parts = tension.split("/");
        String tension_numbers;
        if (tension_parts.length < 2) {
            tension_numbers = tension;
        } else if (tension_parts.length < 3) {
            tension_numbers = String.join("/", tension_parts[0], tension_parts[1]);
        } else {
            tension_numbers = String.join("/", tension_parts[1], tension_parts[2]);
        }
        return tension_numbers;
    }
    public String getTensionUnit(){
        String[] tension_parts = tension.split("/");
        String unit;
        if (tension_parts.length == 3) {
            if (tension_parts[0].equals("lbs")) {
                unit = "lbs";
            } else {
                unit = "kg";
            }
        } else {
            unit = "";
        }
        return unit;
    }
    public void setTension(String tension) {
        this.tension = tension;
    }
    public String getString() {
        return string;
    }
    public void setString(String string) {
        this.string = string;
    }
    public double getDiameter() { return diameter; }
    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }
    public String getStringCrosses() {
        return stringCrosses;
    }
    public void setStringCrosses(String string) {
        this.stringCrosses = string;
    }
    public double getDiameterCrosses() { return diameterCrosses; }
    public void setDiameterCrosses(double diameter) {
        this.diameterCrosses = diameter;
    }
    public void setNote(String note) { this.note = note; }
    public String getNote() { return note; }

    /*
    // print log
    public void printLog() {
        Log.d("MCK",
            "***************** \n " +
            "StringJob \n " +
            "***************** \n " +
            "RacketID: \t" + racketID   +"\n" +
            "Date: \t\t"   + date       +"\n" +
            "Tension: \t"  + tension    +"\n" +
            "String: \t"   + string     +"\n" +
            "Diameter: \t" + diameter   +"\n" +
            "Note: \t\t"   + note       +"\n" +
            "***************** ");
    }
    */

}
