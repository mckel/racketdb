/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.legacy.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    // Needed Classes (Objects) for Design and DB integration.
    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    // Action Bar Menu Items
    private int currentPage;

    // Request codes for db import/export
    private static final int EXPORT_DB = 111;
    private static final int IMPORT_DB = 222;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Remove shadow below action bar
        getSupportActionBar().setElevation(0);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.3
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                currentPage = i;
                invalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }

        });

    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(currentPage != 0) {
            menu.findItem(R.id.action_search).setVisible(false);
            //menu.findItem(R.id.action_addRacket).setVisible(false);
        } else {
            menu.findItem(R.id.action_search).setVisible(true);
            //menu.findItem(R.id.action_addRacket).setVisible(true);
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        //inflater.inflate(R.menu.my, menu);
        //return true;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Handle presses on the action bar items
        switch (id) {
            /*
            case R.id.action_settings:
                openSettings();
               return true;

            case R.id.action_addRacket:
                showAddRacketDialog(getCurrentFocus());
                return true;

            case R.id.action_clear:
                RacketDbHelper dbHelper = new RacketDbHelper(mViewPager.getContext());
                dbHelper.clearAll();
                ListView racketLV = (ListView) findViewById(R.id.lv_rackets);
                RacketListAdapter racketListAdapter = (RacketListAdapter) ((HeaderViewListAdapter)racketLV.getAdapter()).getWrappedAdapter();
                racketListAdapter.changeCursor(null);
                racketListAdapter.notifyDataSetChanged();
            */
            case R.id.action_search:
                EditText etFilter = (EditText) findViewById(R.id.filter_rackets);
                if (etFilter.getVisibility() != View.GONE) {
                    etFilter.setVisibility(View.GONE);
                    // close keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etFilter.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    // clear search
                    etFilter.setText("");

                    // show fab
                    //FloatingActionButton fab_addracket = (FloatingActionButton) findViewById(R.id.fab_addracket);
                    //fab_addracket.show();
                    //fab_addracket.animate().setStartDelay(500).translationYBy(- fab_addracket.getTranslationY() - (float) 16);
                }else{
                    etFilter.setVisibility(View.VISIBLE);
                    etFilter.requestFocus();
                    // show keyboard
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etFilter,InputMethodManager.SHOW_IMPLICIT);
                    // hide fab
                    //FloatingActionButton fab_addracket = (FloatingActionButton) findViewById(R.id.fab_addracket);
                    //fab_addracket.hide();
                }
                return true;

            case R.id.action_about: {
                FragmentManager fmManager = getSupportFragmentManager();
                AboutDialog d = new AboutDialog();
                d.show(fmManager, "AboutDialog");
                return true;
            }

            case R.id.action_exportDB: {

                // Start Export Dialog
                FragmentManager fmManager = getSupportFragmentManager();
                ExportDialog d = new ExportDialog();
                d.show(fmManager, "ExportDialog");
                return true;
            }

            case R.id.action_importDB: {

                // Start Import Dialog
                FragmentManager fmManager = getSupportFragmentManager();
                ImportDialog d = new ImportDialog();
                d.show(fmManager, "ImportDialog");
                return true;
            }
            default:

        }

        return super.onOptionsItemSelected(item);
    }




    /**
     * ******************************************************************************************
     * Methods (Buttons,...)
     */

    // Date format switcher
    public static String getDateUI(String sqlFormat) {

        // first handle "-" values
        if (sqlFormat.equals("-")) {
            return "-";
        }
        else {
            String SQL_FORMAT = "yyyy-MM-dd"; //actually "YYYY-MM-DD HH:MM:SS.SSS" , but still works
            String UI_FORMAT = "d.MM.yyyy";

            SimpleDateFormat sdf = new SimpleDateFormat(SQL_FORMAT);
            Date d = null;
            try {
                d = sdf.parse(sqlFormat);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            sdf.applyPattern(UI_FORMAT);

            return sdf.format(d);
        }
    }

    // String Control (check if string contains some "special" characters)
    public static boolean checkString(String str){
        boolean check = true;
        if (str.contains("'")) {
            check = false;
        }
        return check;

    }

    /*********************************************************************************************
     * Additional Classes
     */

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            // return PlaceholderFragment.newInstance(position + 1);
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new FragmentA();
                    break;
                case 1:
                    fragment = new FragmentB();
                    break;
                case 2:
                    fragment = new FragmentC();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            // Tab Titles:
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }

    }



    public static class AboutDialog extends AppCompatDialogFragment {

        @Override
        public AppCompatDialog onCreateDialog(Bundle paramBundle) {
            super.onCreateDialog(paramBundle);

            // Use Builder to customize dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            //Inflate custom layout and bind to builder
            View v= getActivity().getLayoutInflater().inflate(R.layout.about_dialog, null);
            builder.setView(v);

            // Dialog title
            builder.setTitle(R.string.dialogtitle_about);

            // Next Button
            DialogInterface.OnClickListener listenerPositive = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInt, int paramInt) {
                }
            };
            builder.setPositiveButton(android.R.string.ok, listenerPositive);


            // Set About Message
            // Create dialog with now created settings
            AlertDialog dialog = builder.create();
            // show dialog
            dialog.show();

            // Set Text
            TextView tv = (TextView) dialog.findViewById(R.id.about_versionT);
            tv.setText(R.string.about_versionT);
            tv = (TextView) dialog.findViewById(R.id.about_version);
            tv.setText(BuildConfig.VERSION_NAME);

            tv = (TextView) dialog.findViewById(R.id.about_aboutT);
            tv.setText(R.string.about_aboutT);
            tv = (TextView) dialog.findViewById(R.id.about_about);
            tv.setText(R.string.about_about);

            tv = (TextView) dialog.findViewById(R.id.about_instrT);
            tv.setText(R.string.about_instrT);
            tv = (TextView) dialog.findViewById(R.id.about_instr);
            tv.setText(R.string.about_instr);

            tv = (TextView) dialog.findViewById(R.id.about_contribT);
            tv.setText(R.string.about_contribT);
            tv = (TextView) dialog.findViewById(R.id.about_contrib);
            tv.setText(R.string.about_contrib);

            tv = (TextView) dialog.findViewById(R.id.about_acknowT);
            tv.setText(R.string.about_acknowT);
            tv = (TextView) dialog.findViewById(R.id.about_acknow);
            tv.setText(R.string.about_acknow);

            // return dialog
            return dialog;
        }
    }

    public static class ExportDialog extends AppCompatDialogFragment {

        @Override
        public AppCompatDialog onCreateDialog(Bundle paramBundle) {
            super.onCreateDialog(paramBundle);

            // Use Builder to customize dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.dialogtitle_export);
            builder.setMessage(R.string.export_message);

            // Cancel Button
            DialogInterface.OnClickListener listenerNegative = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            };
            builder.setNegativeButton(R.string.String_Cancel, listenerNegative);

            // Next Button
            DialogInterface.OnClickListener listenerPositive = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInt, int paramInt) {

                    final String fileName = "export.racketdb";
                    Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    intent.putExtra(Intent.EXTRA_TITLE, fileName);
                    //intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, pickerInitialUri);
                    getActivity().startActivityForResult(intent, EXPORT_DB);
                    //myExportActivityResultLauncher.launch(intent);

                }
            };

            builder.setPositiveButton(R.string.export_String, listenerPositive);

            // return dialog
            return builder.create();
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);

        if (requestCode == EXPORT_DB && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                try {
                    RacketDbHelper dbHelper = new RacketDbHelper(getBaseContext());
                    dbHelper.exportDatabase(uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == IMPORT_DB && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (resultData != null) {
                uri = resultData.getData();
                try {
                    RacketDbHelper dbHelper = new RacketDbHelper(getBaseContext());
                    dbHelper.importDatabase(uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public static class ImportDialog extends AppCompatDialogFragment {

        @Override
        public AppCompatDialog onCreateDialog(Bundle paramBundle) {
            super.onCreateDialog(paramBundle);

            // Use Builder to customize dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.dialogtitle_import);
            builder.setMessage(R.string.import_message);

            // Cancel Button
            DialogInterface.OnClickListener listenerNegative = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            };
            builder.setNegativeButton(R.string.String_Cancel, listenerNegative);


            // Next Button
            DialogInterface.OnClickListener listenerPositive = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInt, int paramInt) {

                    final String fileName = "export.racketdb";
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    getActivity().startActivityForResult(intent, IMPORT_DB);

                }
            };
            builder.setPositiveButton(R.string.import_String, listenerPositive);

            // return dialog
            return builder.create();
        }

    }



}
