/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import org.joda.time.DateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by mck on 24.09.14.
 */
public class RacketDbHelper extends SQLiteOpenHelper {
    private Context cont;
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "RacketDatabase.db";
    private final String DATABASE_FILEPATH;

    // Table Names
    public static final String TABLE_RACKETS = "Rackets";
    public static final String TABLE_STRINGJOBS = "Stringjobs";

    // Table Column Names (KEYs)
    // Common column names:
    public static final String KEY_UID = "_id";

    // RACKETS table - column names
    public static final String KEY_OWNER="owner";
    public static final String KEY_MODEL="model";
    public static final String KEY_NUMBER="number";
    public static final String KEY_DEFAULT_TENSION="defaultTension";
    public static final String KEY_DEFAULT_STRING="defaultString";
    public static final String KEY_DEFAULT_DIAMETER="defaultDiameter";
    public static final String KEY_DEFAULT_STRING_CROSS="defaultString_cross";
    public static final String KEY_DEFAULT_DIAMETER_CROSS="defaultDiameter_cross";

    // RACKETS table - string with all keys
    public static final String[] ALL_RACKET_KEYS = new String[] {KEY_UID,KEY_OWNER,KEY_MODEL,KEY_NUMBER,KEY_DEFAULT_TENSION,KEY_DEFAULT_STRING,KEY_DEFAULT_DIAMETER,KEY_DEFAULT_STRING_CROSS,KEY_DEFAULT_DIAMETER_CROSS};


    // STRINGJOBS table - column names
    public static final String KEY_DATE="date";
    public static final String KEY_RACKETID="racketid";
    public static final String KEY_TENSION="tension";
    public static final String KEY_STRING="string";
    public static final String KEY_DIAMETER="diameter";
    public static final String KEY_STRING_CROSS="string_cross";
    public static final String KEY_DIAMETER_CROSS="diameter_cross";
    public static final String KEY_NOTE="note";

    // STRINGJOBS table - string with all keys
    public static final String[] ALL_STRINGJOB_KEYS = new String[] {KEY_UID,KEY_DATE,KEY_RACKETID,KEY_TENSION,KEY_STRING,KEY_DIAMETER,KEY_STRING_CROSS,KEY_DIAMETER_CROSS,KEY_NOTE};

    // Table Create / Delete Statements
    // RACKETS table - strings for flexible database creation
    private static final String CREATE_TABLE_RACKETS = "CREATE TABLE "+TABLE_RACKETS+" ("+
            KEY_UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            KEY_OWNER+" VARCHAR(255), "+
            KEY_MODEL+" VARCHAR(255), "+
            KEY_NUMBER+" INTEGER, "+
            KEY_DEFAULT_TENSION+" VARCHAR(255), "+
            KEY_DEFAULT_STRING+" VARCHAR(255), "+
            KEY_DEFAULT_DIAMETER+" DOUBLE, "+
            KEY_DEFAULT_STRING_CROSS+" VARCHAR(255), "+
            KEY_DEFAULT_DIAMETER_CROSS+" DOUBLE "+
            ");";
    // RACKETS table - string to delete database
    private static final String DELETE_TABLE_RACKTES = "DROP TABLE IF EXISTS " + TABLE_RACKETS;

    // STRINGJOBS table - string for flexible database creation
    private static final String CREATE_TABLE_STRINGJOBS = "CREATE TABLE "+TABLE_STRINGJOBS+" ("+
            KEY_UID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            KEY_DATE+" TEXT, "+
            KEY_RACKETID+" INTEGER, "+
            KEY_TENSION+" VARCHAR(255), "+
            KEY_STRING+" VARCHAR(255), "+
            KEY_DIAMETER+" DOUBLE, "+
            KEY_STRING_CROSS+" VARCHAR(255), "+
            KEY_DIAMETER_CROSS+" DOUBLE, "+
            KEY_NOTE+" VARCHAR(255) "+
            ");";
    // STRINGJOBS table - string to delete database
    private static final String DELETE_TABLE_STRINGJOBS = "DROP TABLE IF EXISTS " + TABLE_STRINGJOBS;


    // Table Version UPDATE Statements
    private static final String ALTER_TABLE_RACKETS_3a = "ALTER TABLE "
            + TABLE_RACKETS + " ADD COLUMN " + KEY_DEFAULT_STRING_CROSS + " VARCHAR(255) DEFAULT ''" + ";";
    private static final String ALTER_TABLE_RACKETS_3b = "ALTER TABLE "
            + TABLE_RACKETS + " ADD COLUMN " + KEY_DEFAULT_DIAMETER_CROSS + " DOUBLE DEFAULT NULL" + ";";
    private static final String ALTER_TABLE_RACKETS_4 = "ALTER TABLE "
            + TABLE_RACKETS + " ADD COLUMN " + KEY_DEFAULT_TENSION + " VARCHAR(255) DEFAULT ''" + ";";
    private static final String ALTER_TABLE_STRINGJOBS_2 = "ALTER TABLE "
            + TABLE_STRINGJOBS + " ADD COLUMN " + KEY_NOTE + " VARCHAR(255) DEFAULT ''" + ";";
    private static final String ALTER_TABLE_STRINGJOBS_3a = "ALTER TABLE "
            + TABLE_STRINGJOBS + " ADD COLUMN " + KEY_STRING_CROSS + " VARCHAR(255) DEFAULT ''" + ";";
    private static final String ALTER_TABLE_STRINGJOBS_3b = "ALTER TABLE "
            + TABLE_STRINGJOBS + " ADD COLUMN " + KEY_DIAMETER_CROSS + " DOUBLE DEFAULT NULL" + ";";


    // Constructor
    public RacketDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        cont = context;
        final String packageName = context.getPackageName();
        DATABASE_FILEPATH = "/data/data/" + packageName + "/databases/" + DATABASE_NAME;
    }

    // Override onCreate method: These is where we need to write create table statements.
    // This is called when database is created.
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create Table Entries (use the strings from above!)
        db.execSQL(CREATE_TABLE_RACKETS);
        db.execSQL(CREATE_TABLE_STRINGJOBS);
        addDefaultEntries(db);
    }


    // Override onUpgrade method: This method is called when database is upgraded like
    // modifying the table structure, adding constraints to database etc.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // See: https://thebhwgroup.com/blog/how-android-sqlite-onupgrade

        if (oldVersion < 2) { // Update to version 2
            db.execSQL(ALTER_TABLE_STRINGJOBS_2);
        }
        if (oldVersion < 3) { // Update to version 3
            db.execSQL(ALTER_TABLE_RACKETS_3a);
            db.execSQL(ALTER_TABLE_RACKETS_3b);
            db.execSQL(ALTER_TABLE_STRINGJOBS_3a);
            db.execSQL(ALTER_TABLE_STRINGJOBS_3b);
        }
        if (oldVersion < 4) { // Update to version 4
            db.execSQL(ALTER_TABLE_RACKETS_4);
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }


    private void addDefaultEntries(SQLiteDatabase db) {
        // Dummy database entries
        addRacketDef(db, new Racket("Carlos Alcaraz", "Babolat Pure Aero VS", 1, "kg/25.0/24.0", "Babolat RPM Blast", 1.30));
        addRacketDef(db, new Racket("Carlos Alcaraz", "Babolat Pure Aero VS", 2, "kg/25.0/24.0", "Babolat RPM Blast", 1.30));
        addRacketDef(db, new Racket("Carlos Alcaraz", "Babolat Pure Aero VS", 3, "kg/25.0/24.0", "Babolat RPM Blast", 1.30));
        addRacketDef(db, new Racket("Iga Swiatek", "Tecnifibre Tempo 298 IGA", 1, "kg/24.0/23.5", "Tecnifibre Razor Code", 1.25));
        addRacketDef(db, new Racket("Iga Swiatek", "Tecnifibre Tempo 298 IGA", 2, "kg/24.0/23.5", "Tecnifibre Razor Code", 1.25));

        // Date:
        String SQL_FORMAT = "yyyy-MM-dd"; //actually "YYYY-MM-DD HH:MM:SS.SSS" , but still works
        SimpleDateFormat sdf = new SimpleDateFormat(SQL_FORMAT);
        DateTime cTime = new DateTime();

        addStringjobDef(db, new StringJob(1, sdf.format(cTime.toDate()) +                " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(1, sdf.format(cTime.minusDays(5).toDate()) +   " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(1, sdf.format(cTime.minusDays(15).toDate()) +  " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(2, sdf.format(cTime.minusDays(17).toDate()) +  " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(2, sdf.format(cTime.minusDays(23).toDate()) +  " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(3, sdf.format(cTime.minusDays(55).toDate()) +  " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(3, sdf.format(cTime.minusDays(59).toDate()) +  " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(3, sdf.format(cTime.minusDays(72).toDate()) +  " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(3, sdf.format(cTime.minusDays(99).toDate()) +  " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(3, sdf.format(cTime.minusDays(104).toDate()) + " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(3, sdf.format(cTime.minusDays(105).toDate()) + " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));
        addStringjobDef(db, new StringJob(3, sdf.format(cTime.minusDays(133).toDate()) + " 11:11:11.111", "kg/25.0/24.0", "Babolat Pure Aero VS",1.30,""));

        addStringjobDef(db, new StringJob(4, sdf.format(cTime.minusDays(1).toDate()) +   " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(4, sdf.format(cTime.minusDays(2).toDate()) +   " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(4, sdf.format(cTime.minusDays(6).toDate()) +   " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(4, sdf.format(cTime.minusDays(22).toDate()) +  " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(25).toDate()) +  " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(27).toDate()) +  " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(40).toDate()) +  " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(44).toDate()) +  " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(77).toDate()) +  " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(78).toDate()) +  " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(102).toDate()) + " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(113).toDate()) + " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(110).toDate()) + " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));
        addStringjobDef(db, new StringJob(5, sdf.format(cTime.minusDays(111).toDate()) + " 11:11:11.111", "kg/24.0/23.5", "Tecnifibre Razor Code",1.25,""));

    }

    /**
     * ******************************************************************************************
     * CRUD Operations: (Create, Read, Update and Delete)
     * for GENERAL DATABASE
     */
    public void clearAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Discard the data and start over
        db.execSQL(DELETE_TABLE_RACKTES);
        db.execSQL(DELETE_TABLE_STRINGJOBS);
        onCreate(db);
        // close db
        db.close();
    }

    /**
     * ******************************************************************************************
     * CRUD Operations: (Create, Read, Update and Delete)
     * for RACKETS TABLE
     */

    // Add one new racket entry
    public void addRacket(Racket racket) {
        // open database connection
        SQLiteDatabase db = this.getWritableDatabase();

        // assign values
        ContentValues values = new ContentValues();
        values.put(KEY_OWNER,racket.getOwner());
        values.put(KEY_MODEL,racket.getModel());
        values.put(KEY_NUMBER,racket.getNumber());
        values.put(KEY_DEFAULT_TENSION,racket.getDefaultTension());
        values.put(KEY_DEFAULT_STRING, racket.getDefaultString());
        values.put(KEY_DEFAULT_DIAMETER, racket.getDefaultDiameter());
        values.put(KEY_DEFAULT_STRING_CROSS, racket.getDefaultCrossString());
        values.put(KEY_DEFAULT_DIAMETER_CROSS, racket.getDefaultCrossDiameter());

        // insert row to database
        db.insert(TABLE_RACKETS, null, values); //here the ID is added automatically! (unique)
        // (returns -1 if failed)

        // close database connection
        db.close();
    }

    // Add one new racket entry
    public void addRacketDef(SQLiteDatabase db,Racket racket) {

        // assign values
        ContentValues values = new ContentValues();
        values.put(KEY_OWNER,racket.getOwner());
        values.put(KEY_MODEL,racket.getModel());
        values.put(KEY_NUMBER,racket.getNumber());
        values.put(KEY_DEFAULT_TENSION,racket.getDefaultTension());
        values.put(KEY_DEFAULT_STRING,racket.getDefaultString());
        values.put(KEY_DEFAULT_DIAMETER, racket.getDefaultDiameter());
        values.put(KEY_DEFAULT_STRING_CROSS,racket.getDefaultCrossString());
        values.put(KEY_DEFAULT_DIAMETER_CROSS, racket.getDefaultCrossDiameter());

        // insert row to database
        db.insert(TABLE_RACKETS, null, values); //here the ID is added automatically! (unique)
    }

    // Get data of one racket with specific KEY_UID
    public Racket getRacket(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_RACKET_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = null;
        // Selection (KEY_UID = (value of next parameter)
        String selection = KEY_UID + "=?";
        // SelectionArgs (KEY_UID of the searched racket)
        String[] selectionArgs = {String.valueOf(id)};
        Cursor c = null;
        c = db.query(
                TABLE_RACKETS,   // The table to query
                projection,      // The columns to return
                selection,       // The columns for the WHERE clause
                selectionArgs,   // The values for the WHERE clause
                null,    // don't group the rows
                null,     // don't filter by row groups
                sortOrder       // The sort order
        );

        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry
        }

        // Create racket object
        Racket racket = new Racket(
                c.getInt(0),                        // id
                c.getString(1),                     // owner
                c.getString(2),                     // model
                Integer.parseInt(c.getString(3)),   // number
                c.getString(4),                     // defaultTension
                c.getString(5),                     // defaultString
                c.getDouble(6),                     // defaultDiameter
                c.getString(7),                     // defaultCrossString
                c.getDouble(8)                      // defaultCrossDiameter
        );

        //close things
        c.close();
        db.close();

        return racket;
    }

    // Get all data in the database.
    public Cursor getAllRackets() {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_RACKET_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = null;
        // Selection (KEY_UID = (value of next parameter)
        String selection = null;
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null;
        Cursor c = null;
        c = db.query(
                TABLE_RACKETS,          // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry
        }

        // close things
        db.close();

        return c;
    }

    // Update a single racket in database
    public void updateRacket(Racket racket) {
        // open database connection
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // assign values
        values.put(KEY_OWNER,racket.getOwner());
        values.put(KEY_MODEL,racket.getModel());
        values.put(KEY_NUMBER,racket.getNumber());
        values.put(KEY_DEFAULT_TENSION,racket.getDefaultTension());
        values.put(KEY_DEFAULT_STRING,racket.getDefaultString());
        values.put(KEY_DEFAULT_DIAMETER,racket.getDefaultDiameter());
        values.put(KEY_DEFAULT_STRING_CROSS,racket.getDefaultCrossString());
        values.put(KEY_DEFAULT_DIAMETER_CROSS,racket.getDefaultCrossDiameter());

        // get KEY_UID
        int id = racket.getId();

        // UPDATE row in database
        db.update(TABLE_RACKETS, values, KEY_UID + "=" + id, null);
        //db.update(TABLE_NAME,values,KEY_UID + "= ?", new String[] { String.valueOf(racket.getId()) });
        // (returns -1 if failed)

        // close database connection
        db.close();
    }

    // Delete one racket entry (id as parameter)
    public void deleteRacket(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        // DELETE row in database
        db.delete(TABLE_RACKETS, KEY_UID + "=" + id, null);

        // close database connection
        db.close();
    }

    // Delete all entries
    public void clearAllRackets() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Discard the data and start over
        db.execSQL(DELETE_TABLE_RACKTES);
        db.execSQL(CREATE_TABLE_RACKETS);
        // close db
        db.close();
    }

    // Get db size (count)
    public int getRacketsCount(){
        Cursor c = getAllRackets();
        int count = c.getCount();

        // close things
        c.close();

        return count;
    }

    // Filter Owner by string
    public Cursor getAllRacketsnameContain(String filterStr) {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_RACKET_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = KEY_OWNER +", " + KEY_MODEL +", " + KEY_NUMBER;
        // Selection (KEY_UID = (value of next parameter)
        String selection = KEY_OWNER +" LIKE  " + "'%"+filterStr+"%'" + " OR " + KEY_MODEL + " LIKE  " + "'%"+filterStr+"%'";
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null; // If filtering only one column: selection = KEY_OWNER + " LIKE ? " ; selectioArgs = {"%"+filterStr+"%"};


        // if filter is empty
        if (filterStr == null || filterStr.isEmpty()) {
            // Return full list
            return getAllRacketsAlphabetic();
        }
        else {
            Cursor c = null;
            // Else filter
            c = db.query(
                    TABLE_RACKETS,          // The table to query
                    projection,             // The columns to return
                    selection,              // The columns for the WHERE clause
                    selectionArgs,          // The values for the WHERE clause
                    null,                   // don't group the rows
                    null,                   // don't filter by row groups
                    sortOrder               // The sort order
            );
            // check if query was successful
            if (c != null) {
                c.moveToFirst(); // move to first entry (should be only one)
            }

            // close things
            db.close();

            return c;
        }
    }

    // All Rackets, sorted by owner
    public Cursor getAllRacketsAlphabetic() {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_RACKET_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = KEY_OWNER + ", " + KEY_MODEL + ", " + KEY_NUMBER;
        // Selection (KEY_UID = (value of next parameter)
        String selection = null;
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null;


        Cursor c = null;
        // Else filter
        c = db.query(
                TABLE_RACKETS,          // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }

        // close things
        db.close();

        return c;
    }

    // Return list for spinner in history fragment
    public List<String> getAllRacketsSpinner() {
        // Create String list
        List<String> labels = new ArrayList<String>();

        // Query entries from database
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_RACKET_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = KEY_OWNER + ", " + KEY_MODEL;
        // Selection (KEY_UID = (value of next parameter)
        String selection = null;
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null;


        Cursor c = null;
        // Else filter
        c = db.query(
                true,                   // Distinct (boolean)
                TABLE_RACKETS,          // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                KEY_MODEL +", "+ KEY_OWNER,    // don't group the rows
                null,                   // don't filter by row groups
                sortOrder,              // The sort order
                null                    // limit
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }

        if (c.moveToFirst()) {
            do {
                labels.add(c.getString(c.getColumnIndex(KEY_MODEL)) +" "+ cont.getResources().getString(R.string.by) +" "+c.getString(c.getColumnIndex(KEY_OWNER)));
            } while (c.moveToNext());
        }

        if (c.getCount()==0){ //same as c.moveToFirst() == false
            labels.add(cont.getResources().getString(R.string.pleaseAddRacket));
        }

        // close things
        c.close();
        db.close();

        return labels;
    }

    // Return list with stringjobs by racketNr
    public List<Integer> getAllStringJobsSpinner(int itemListID) {
        // Create int arraylist
        List<Integer> idList = new ArrayList<Integer>();
        List<Integer> list = new ArrayList<Integer>();
        list.add(0); // fist position 0 (due to plot settings)

        // Query entries from database
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_RACKET_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = KEY_OWNER + ", " + KEY_MODEL;
        // Selection (KEY_UID = (value of next parameter)
        String selection = null;
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null;

        Cursor c = null;
        // Else filter
        c = db.query(
                true,                   // Distinct (boolean)
                TABLE_RACKETS,          // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                KEY_MODEL +", "+ KEY_OWNER,    // don't group the rows
                null,                   // don't filter by row groups
                sortOrder,              // The sort order
                null                    // limit
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }

        if(c.getCount()==0) {
            // quit, since there are no rackets
            list.add(0);
            list.add(0);
            return list;
        }
        c.moveToPosition(itemListID); //
        String model = c.getString(c.getColumnIndex(KEY_MODEL));
        String owner = c.getString(c.getColumnIndex(KEY_OWNER));


        // Find all rackets with given model and owner

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection2 = {KEY_UID,KEY_NUMBER};
        // How you want the results sorted in the resulting Cursor
        sortOrder = KEY_NUMBER;
        // Selection (KEY_UID = (value of next parameter)
        selection = "( "+ KEY_MODEL + " = '" +model+ "' AND "+ KEY_OWNER +" = '"+owner +"' )";
        // SelectionArgs (no argument--> all)
        String[] selectionArgs2 = null;
        c.close();
        // Else filter
        c = db.query(
                TABLE_RACKETS,          // The table to query
                projection2,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs2,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }


        if (c.moveToFirst()) {
            do {
                idList.add(c.getInt(c.getColumnIndex(KEY_UID)));
            } while (c.moveToNext());
        }

        if (c.getCount()==0){ //same as c.moveToFirst() == false
        }

        c.close();

        // Now all racket IDs are known --> get stringjobs

        for (int k = 0; k < idList.size(); k++) {
            c = getAllStringJobRacket(idList.get(k));
            c.moveToFirst();

            // Add number of stringjobs in database to list
            list.add(c.getCount());

            // close cursor
            c.close();
        }

        // close things
        db.close();

        list.add(0); // last position 0 (due to plot settings)

        return list;
    }

    // Stringjobs of last six month (for Plot3)
    public List<Integer> getMonthlyStringJobsSpinnerSixMonth(int itemListID) {
        // Create int arraylist
        List<Integer> list = new ArrayList<Integer>();
        List<Integer> idList = new ArrayList<Integer>();


        // Query entries from database
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_RACKET_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = KEY_OWNER + ", " + KEY_MODEL;
        // Selection (KEY_UID = (value of next parameter)
        String selection = null;
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null;

        Cursor c = null;
        // Else filter
        c = db.query(
                true,                   // Distinct (boolean)
                TABLE_RACKETS,          // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                KEY_MODEL +", "+ KEY_OWNER,    // don't group the rows
                null,                   // don't filter by row groups
                sortOrder,              // The sort order
                null                    // limit
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }

        if(c.getCount()==0) {
            // quit, since there are no rackets
            for(int m = 1;m<=6;m++) {
                list.add(0);
            }
            return list;
        }
        c.moveToPosition(itemListID); //
        String model = c.getString(c.getColumnIndex(KEY_MODEL));
        String owner = c.getString(c.getColumnIndex(KEY_OWNER));

        // Find all rackets with given model and owner

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection2 = {KEY_UID,KEY_NUMBER};
        // How you want the results sorted in the resulting Cursor
        sortOrder = KEY_NUMBER;
        // Selection (KEY_UID = (value of next parameter)
        selection = "( "+ KEY_MODEL + " = '" +model+ "' AND "+ KEY_OWNER +" = '"+owner +"' )";
        // SelectionArgs (no argument--> all)
        String[] selectionArgs2 = null;
        c.close();
        // Else filter
        c = db.query(
                TABLE_RACKETS,          // The table to query
                projection2,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs2,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }

        if (c.moveToFirst()) {
            do {
                idList.add(c.getInt(c.getColumnIndex(KEY_UID)));
            } while (c.moveToNext());
        }

        if (c.getCount()==0){ //same as c.moveToFirst() == false
        }

        c.close();

        // Now all racket IDs are known --> get sixMonth

        List<Integer> tempList = new ArrayList<Integer>();
        for (int k = 0; k < idList.size(); k++) {

            tempList = getCountOfStringJobsRacketByLastSixMonth(idList.get(k));

            // Add number of stringjobs in database to list
            for (int l = 0; l<tempList.size();l++) {
                if(k==0) {
                    list.add(tempList.get(l));
                }
                else {
                    list.set(l, list.get(l)+tempList.get(l));
                }
            }

        }

        // close things
        db.close();

        return list;

    }

    // Return all Racket Numbers for a given name and owner
    public List<Integer> getRacketNumbersNameOwner(int itemListID) {
        // Create int arraylist
        List<Integer> list = new ArrayList<Integer>();
        List<Integer> idList = new ArrayList<Integer>();


        // Query entries from database
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_RACKET_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = KEY_OWNER + ", " + KEY_MODEL;
        // Selection (KEY_UID = (value of next parameter)
        String selection = null;
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null;

        Cursor c = null;
        // Else filter
        c = db.query(
                true,                   // Distinct (boolean)
                TABLE_RACKETS,          // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                KEY_MODEL +", "+ KEY_OWNER,    // don't group the rows
                null,                   // don't filter by row groups
                sortOrder,              // The sort order
                null                    // limit
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }

        if(c.getCount()==0) {
            // quit, since there are no rackets
            for(int m = 1;m<=6;m++) {
                list.add(0);
            }
            return list;
        }
        c.moveToPosition(itemListID); //
        String model = c.getString(c.getColumnIndex(KEY_MODEL));
        String owner = c.getString(c.getColumnIndex(KEY_OWNER));

        // Find all rackets with given model and owner

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection2 = {KEY_UID,KEY_NUMBER};
        // How you want the results sorted in the resulting Cursor
        sortOrder = KEY_NUMBER;
        // Selection (KEY_UID = (value of next parameter)
        selection = "( "+ KEY_MODEL + " = '" +model+ "' AND "+ KEY_OWNER +" = '"+owner +"' )";
        // SelectionArgs (no argument--> all)
        String[] selectionArgs2 = null;
        c.close();
        // Else filter
        c = db.query(
                TABLE_RACKETS,          // The table to query
                projection2,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs2,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }

        if (c.moveToFirst()) {
            do {
                idList.add(c.getInt(c.getColumnIndex(KEY_UID)));
            } while (c.moveToNext());
        }

        if (c.getCount()==0){ //same as c.moveToFirst() == false
        }

        c.close();

        // Now all racket IDs are known --> get Racket Numbers
        Racket tmpRacket;
        list.add(0); // due to plot style
        for (int k = 0; k < idList.size(); k++) {
            tmpRacket = getRacket(idList.get(k));
            list.add(tmpRacket.getNumber());
        }
        list.add(0); // due to plot style

        // close things
        db.close();

        return list;

    }


    public boolean checkRacket(Racket r) {
        boolean exists;

        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {KEY_UID};
        // How you want the results sorted in the resulting Cursor
        String sortOrder = null;
        // Selection (KEY_UID = (value of next parameter)
        String selection = KEY_MODEL + " = '"+r.getModel()+"' AND "+ KEY_OWNER + " = '"+r.getOwner()+"' AND " +KEY_NUMBER +" = '"+Integer.toString(r.getNumber())+"'";
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null;


        Cursor c = null;
        // Else filter
        c = db.query(
                TABLE_RACKETS,          // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }

        // close things
        db.close();

        // Check if racket exists?
        exists = (c.getCount() != 0);

        return exists;

    }



    /**
     * ******************************************************************************************
     * CRUD Operations: (Create, Read, Update and Delete)
     * for STRINGJOBS TABLE
     */

    // Add one new stringjob entry
    public void addStringjob(StringJob stringjob) {
        // open database connection
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // assign values
        values.put(KEY_DATE,stringjob.getDate());
        values.put(KEY_RACKETID,stringjob.getRacketID());
        values.put(KEY_TENSION,stringjob.getTension());
        values.put(KEY_STRING,stringjob.getString());
        values.put(KEY_DIAMETER,stringjob.getDiameter());
        values.put(KEY_STRING_CROSS,stringjob.getStringCrosses());
        values.put(KEY_DIAMETER_CROSS,stringjob.getDiameterCrosses());
        values.put(KEY_NOTE,stringjob.getNote());
        // insert row to database
        db.insert(TABLE_STRINGJOBS, null, values); //here the ID is added automatically! (unique)
        // (returns -1 if failed)
        // close database connection
        db.close();
    }

    // Add one new stringjob entry
    public void addStringjobDef(SQLiteDatabase db,StringJob stringjob) {

        ContentValues values = new ContentValues();
        // assign values
        values.put(KEY_DATE,stringjob.getDate());
        values.put(KEY_RACKETID,stringjob.getRacketID());
        values.put(KEY_TENSION,stringjob.getTension());
        values.put(KEY_STRING,stringjob.getString());
        values.put(KEY_DIAMETER,stringjob.getDiameter());
        values.put(KEY_STRING_CROSS,stringjob.getStringCrosses());
        values.put(KEY_DIAMETER_CROSS,stringjob.getDiameterCrosses());
        values.put(KEY_NOTE,stringjob.getNote());
        // insert row to database
        db.insert(TABLE_STRINGJOBS, null, values); //here the ID is added automatically! (unique)
    }

    // Delete one stringjob entry (id as parameter)
    public void deleteStringjob(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        // DELETE row in database
        db.delete(TABLE_STRINGJOBS, KEY_UID + "=" + id, null);

        // close database connection
        db.close();
    }

    // Delete all entries
    public void clearAllStringjobs() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Discard the data and start over
        db.execSQL(DELETE_TABLE_STRINGJOBS);
        db.execSQL(CREATE_TABLE_STRINGJOBS);
        // close db
        db.close();
    }

    // Get all data in the database.
    public Cursor getAllStringjobs() {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_STRINGJOB_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = "date("+KEY_DATE + ") DESC, "+KEY_UID + " DESC";
        // Selection (KEY_UID = (value of next parameter)
        String selection = null;
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null;
        Cursor c = null;
        c = db.query(
                TABLE_STRINGJOBS,   // The table to query
                projection,         // The columns to return
                selection,          // The columns for the WHERE clause
                selectionArgs,      // The values for the WHERE clause
                null,       // don't group the rows
                null,        // don't filter by row groups
                sortOrder           // The sort order
        );
        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }

        // close things
        db.close();

        return c;
    }

    // Get data of one stringjob with specific KEY_UID
    public StringJob getStringjob(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_STRINGJOB_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = null;
        // Selection (KEY_UID = (value of next parameter)
        String selection = KEY_UID + "=?";
        // SelectionArgs (KEY_UID of the searched racket)
        String[] selectionArgs = {String.valueOf(id)};
        Cursor c = null;
        c = db.query(
                TABLE_STRINGJOBS,       // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        // check if query was successful
        if (c != null) {
            c.moveToFirst(); // move to first entry (should be only one)
        }


        // Create stringjob object object
        StringJob stringjob = new StringJob(
                Integer.parseInt(c.getString(0)),   // id (int)
                Integer.parseInt(c.getString(2)),   // racketID (int)
                c.getString(1),                     // date (String)
                c.getString(3),                     // tension (String)
                c.getString(4),                     // string (String)
                Double.parseDouble(c.getString(5)), // string diameter (double)
                c.getString(5),                     // cross string (String)
                Double.parseDouble(c.getString(6)), // cross string diameter (double)
                c.getString(7)                      // note (String)
        );

        //close things
        c.close();
        db.close();

        return stringjob;
    }

    // Get db size (count)
    public int getStringjobsCount(){
        Cursor c = getAllStringjobs();
        int count = c.getCount();

        // close things
        c.close();

        return count;
    }

    // Return latest stringjob corresponding to the db ID of a racket.
    public StringJob getLastStringJobRacket(int racketID) {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_STRINGJOB_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = KEY_DATE;
        //String sortOrder = null;
        // Selection (KEY_UID = (value of next parameter)
        String selection = KEY_RACKETID + "=?";
        //String selection = null;
        // SelectionArgs (KEY_UID of the searched racket)
        String[] selectionArgs = {String.valueOf(racketID)};
        //String[] selectionArgs = null;
        Cursor c = null;
        c = db.query(
                TABLE_STRINGJOBS,       // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        // check if query was successful
        if (c != null) {
            c.moveToLast(); // move to last entry
        }

        //DB not containing any relevant stringjob (or error)
        if (c.getCount() == 0) {
            // Create dummy stringjob
            StringJob stringjob = new StringJob(
                    0,       // id (int)
                    0,          // racketID (int)
                    "-",        // date (String)
                    "-",        // tension (String)
                    "-",        // string (String)
                    Double.NaN, // string diameter (double)
                    "-",        // crosses string (String)
                    Double.NaN, // crosses string diameter (double)
                    "-"         // Note (String)
            );
            c.close();
            db.close();
            return stringjob;
        }

        // Create stringjob object
        StringJob stringjob = new StringJob(
                Integer.parseInt(c.getString(0)),   // id (int)
                Integer.parseInt(c.getString(2)),   // racketID (int)
                c.getString(1),                     // date (String)
                c.getString(3),                     // tension (String)
                c.getString(4),                     // string (String)
                c.getDouble(5),                     // string diameter (double)
                c.getString(6),                     // cross string (String)
                c.getDouble(7),                     // cross string diameter (double)
                c.getString(8)                      // note (String)
        );

        //close things
        c.close();
        db.close();

        return stringjob;
    }



    // Return all stringjobs corresponding to the db ID of a racket.
    public Cursor getAllStringJobRacket(int racketID) {
        SQLiteDatabase db = this.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = ALL_STRINGJOB_KEYS;
        // How you want the results sorted in the resulting Cursor
        String sortOrder = null;
        // Selection (KEY_UID = (value of next parameter)
        String selection = KEY_RACKETID + "=?";
        // SelectionArgs (KEY_UID of the searched racket)
        String[] selectionArgs = {String.valueOf(racketID)};
        Cursor c = null;
        c = db.query(
                TABLE_STRINGJOBS,       // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        // check if query was successful
        if (c != null) {
            c.moveToLast(); // move to last entry
        }

        // close things
        db.close();

        return c;
    }


    // Get stringjobs counted and grouped by month
    private List<Integer> getCountOfStringJobsRacketByLastSixMonth(Integer racketID) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Integer> list = new ArrayList<Integer>();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection ={KEY_RACKETID};
        // How you want the results sorted in the resulting Cursor
        String sortOrder = null;
        // Selection (KEY_UID = (value of next parameter)
        String selection;
        // SelectionArgs (no argument--> all)
        String[] selectionArgs = null;
        Cursor c = null;

        for(int k = 6;k>0;k--) {

            switch (k) {
                case 6:
                    selection = KEY_RACKETID+" = '" +racketID+ "' AND date("+KEY_DATE+") BETWEEN date('now','start of month','-5 month') AND date('now','start of month','-4 month','-1 day') ";
                    break;
                case 5:
                    selection = KEY_RACKETID+" = '" +racketID+ "' AND date("+KEY_DATE+") BETWEEN date('now','start of month','-4 month') AND date('now','start of month','-3 month','-1 day') ";
                    break;
                case 4:
                    selection = KEY_RACKETID+" = '" +racketID+ "' AND date("+KEY_DATE+") BETWEEN date('now','start of month','-3 month') AND date('now','start of month','-2 month','-1 day') ";
                    break;
                case 3:
                    selection = KEY_RACKETID+" = '" +racketID+ "' AND date("+KEY_DATE+") BETWEEN date('now','start of month','-2 month') AND date('now','start of month','-1 month','-1 day') ";
                    break;
                case 2:
                    selection = KEY_RACKETID+" = '" +racketID+ "' AND date("+KEY_DATE+") BETWEEN date('now','start of month','-1 month') AND date('now','start of month','-1 day') ";
                    break;
                case 1:
                    selection = KEY_RACKETID+" = '" +racketID+ "' AND date("+KEY_DATE+") BETWEEN date('now','start of month') AND date('now','start of month','+1 month','-1 day') ";
                    break;
                default:
                    selection ="";
                    break;
            }

            c = db.query(
                    TABLE_STRINGJOBS,       // The table to query
                    projection,             // The columns to return
                    selection,              // The columns for the WHERE clause
                    selectionArgs,          // The values for the WHERE clause
                    null,           // don't group the rows
                    null,            // don't filter by row groups
                    sortOrder               // The sort order
            );

            list.add(c.getCount());

            c.close();

        }

        // close things
        db.close();

        return list;

    }


    /**
     * Copies the database file at the specified location
     * over the current internal application database.
     * */
    public boolean importDatabase(Uri inFile) throws IOException {

        // Close the SQLiteOpenHelper so it will
        // commit the created empty database to internal storage.
        close();
        File newDb = new File(inFile.getPath());
        File oldDb = new File(DATABASE_FILEPATH);

        try {
            FileInputStream newDBstream = new FileInputStream(cont.getContentResolver().openFileDescriptor(inFile, "r").getFileDescriptor());
            //copyFile(new FileInputStream(newDb), new FileOutputStream(oldDb));
            copyFile(newDBstream, new FileOutputStream(oldDb));
            // Access the copied database so SQLiteHelper
            // will cache it and mark it as created.
            getWritableDatabase().close();
            Toast.makeText(cont, "Database successfully imported!", Toast.LENGTH_SHORT).show();
            return true;

        } catch (IOException e) {
            Toast.makeText(cont, "Something went wrong! Database not imported!", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return false;
        }
    }

    private void copyFile(FileInputStream fromFile, FileOutputStream toFile) throws IOException {
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try {
            fromChannel = fromFile.getChannel();
            toChannel = toFile.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }

    public void exportDatabase(Uri outFile) throws IOException {

        // Open your local db as the input stream
        String inFileName = DATABASE_FILEPATH;
        File dbFile = new File(inFileName);
        FileInputStream fis = new FileInputStream(dbFile);

        // transfer bytes from the inputfile to the outputfile
        OutputStream output = cont.getContentResolver().openOutputStream(outFile);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }
        // Close the streams
        output.flush();
        output.close();
        fis.close();
        
        Toast.makeText(cont, "Database successfully exported!", Toast.LENGTH_LONG).show();

    }



    /*
    //Print for debugging
    private void printCursorStringJob(Cursor c) {
        if (c != null) {
            c.moveToFirst();
            int count = c.getCount();
            for (int k = 1; k <= count; k++) {
                Log.d("MCK", "*****************\n" +
                        "Cursor\n" +
                        "*****************\n" +
                        "id\t\t"    + c.getString(0) + "\n" +
                        "racketID\t"+ c.getString(2) + "\n" +
                        "date\t\t"  + c.getString(1) + "\n" +
                        "tension\t" + c.getString(3) + "\n" +
                        "string\t"  + c.getString(4) + "\n" +
                        "diameter\t"+ c.getString(5) + "\n" +
                        "*****************");
                c.moveToNext();
            }
        } else {
            Log.d("MCK", "Cursor == null");
        }
    }

    private void printCursorRacket(Cursor c) {
        if (c != null) {
            c.moveToFirst();
            int count = c.getCount();
            for (int k = 1; k <= count; k++) {
                Log.d("MCK", "*****************\n" +
                        "Cursor\n" +
                        "*****************\n" +
                        "id\t\t"    + c.getString(0) + "\n" +
                        "owner\t" + c.getString(1) + "\n" +
                        "model\t"  + c.getString(2) + "\n" +
                        "number\t" + c.getString(3) + "\n" +
                        "*****************");
                c.moveToNext();
            }
        } else {
            Log.d("MCK", "Cursor == null");
        }
    }
    */

}

