# RacketDB

![App banner](additional_assets/playstore-assets/banner2.png)

Hi! Thanks for your interest in this project and your contribution. :tennis: :muscle:

## About

I initially created this project back in 2014 to learn how app development works - I just wanted to know what's going on *behind the scenes*.
The idea to create an app to organize the string jobs of tennis rackets, was inspired by my passion for playing tennis and the need to keep an overview of the several rackets in my tennis bag. Ever since, I kept updating the app from time to time in my free time and released the code as open-source in 2018.

## Repository

This Git repository contains the code for the Android app [RacketDB](https://play.google.com/store/apps/details?id=com.marc_keller.racketdb).
The code is edited with the IDE Android Studio.

## License

This software is released under the [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0).

```
Copyright 2018-2023 Marc C. Keller

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```

## Contact

RacketDB is maintained by Marc Keller.

* GitLab: @mckel
* Email: racketdb@gmail.com
