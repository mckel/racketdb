/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

/**
 * Created by mck on 26.09.14.
 */
public class RacketListAdapter extends CursorAdapter {

    // declare inflater
    private LayoutInflater mInflater;


    // Constructor
    public RacketListAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        // Initialize inflater
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * ******************************************************************************************
     * Override Methods
     */

    // newView method is called to create a View object representing on item in the list,
    // here You just create an object don't set any values
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        // LayoutInflater creates a view based on xml structure in resource file
        // passed to inflate method as first parameter.

        return mInflater.inflate(R.layout.listview_rackets, parent, false);

    }

    // View returned from newView is passed as first parameter to bindView,
    // it is here where You will set values to display. (bindData)
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        /**
         * This is where the action takes place
         */
        //First You get element to populate by calling view.findViewById(),
        // cast it to correct type,...
        //... and set value.
        TextView v_model = (TextView) view.findViewById(R.id.lstV_rackets_model);
        v_model.setText(cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_MODEL)));

        TextView v_owner = (TextView) view.findViewById(R.id.lstV_rackets_owner);
        v_owner.setText(cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_OWNER)));

        //TextView v_number = (TextView) view.findViewById(R.id.lstV_rackets_number);
        //v_number.setText(cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_NUMBER)));

        // Also data from string db needed
        RacketDbHelper dbHelper = new RacketDbHelper(context);
        StringJob lastStringJob = dbHelper.getLastStringJobRacket(cursor.getInt(cursor.getColumnIndex(RacketDbHelper.KEY_UID)));

        // Set date
        TextView v_date = (TextView) view.findViewById(R.id.lstV_rackets_date);
        v_date.setText(MainActivity.getDateUI(lastStringJob.getDate()));

        // Set tension
        TextView v_tension = (TextView) view.findViewById(R.id.lstV_rackets_tension);
        ImageView v_weightIcon = (ImageView) view.findViewById(R.id.lstV_rackets_weightImage);
        v_tension.setText(lastStringJob.getTensionNumbers());
        int weight_icon;
        if (lastStringJob.getTensionUnit().equals("lbs")) {
            weight_icon = R.drawable.icon_weight_lbs;
        } else {
            weight_icon= R.drawable.icon_weight;
        }
        v_weightIcon.setImageResource(weight_icon);

        // Set racket number
        int racketNumber = cursor.getInt(cursor.getColumnIndex(RacketDbHelper.KEY_NUMBER));
        TextView v_racketNumber = (TextView) view.findViewById(R.id.lstV_rackets_numberValue);
        v_racketNumber.setText(String.valueOf(racketNumber));


        // Detect Brand
        int brandID = 0;
        String modelName = cursor.getString(cursor.getColumnIndex(RacketDbHelper.KEY_MODEL));
        if (modelName.toLowerCase().contains("wilson")) {
            brandID=1;
        }
        else if (modelName.toLowerCase().contains("yonex")) {
            brandID=3;
        }
        else if (modelName.toLowerCase().contains("babolat")) {
            brandID=4;
        }
        else if (modelName.toLowerCase().contains("donnay")) {
            brandID=5;
        }
        else if (modelName.toLowerCase().contains("dunlop")) {
            brandID=6;
        }
        else if (modelName.toLowerCase().contains("pacific")) {
            brandID=7;
        }
        else if (modelName.toLowerCase().contains("prince")) {
            brandID=8;
        }
        else if (modelName.toLowerCase().contains("solinco")) {
            brandID=9;
        }
        else if (modelName.toLowerCase().contains("tecnifibre")) {
            brandID=10;
        }
        else if (modelName.toLowerCase().contains("völkl") || modelName.toLowerCase().contains("voelkl") || modelName.toLowerCase().contains("volkl") ) {
            brandID=11;
        }
        else if (modelName.toLowerCase().contains("slazenger")) {
            brandID=12;
        }
        else if (modelName.toLowerCase().contains("prokennex")) {
            brandID=13;
        }
        else if (modelName.toLowerCase().contains("artengo")) {
            brandID=14;
        }
        else if (modelName.toLowerCase().contains("ashaway")) {
            brandID = 15;
        } else if (modelName.toLowerCase().contains("carlton")) {
            brandID = 16;
        } else if (modelName.toLowerCase().contains("yehlex")) {
            brandID = 17;
        } else if (modelName.toLowerCase().contains("apacs")) {
            brandID = 18;
        }
        else if (modelName.toLowerCase().contains("head")) {// Search for head last, since it could occur in e.g. Overhead
            brandID=2;
        }
        else if (modelName.toLowerCase().contains("kontaktlinsen") || modelName.toLowerCase().contains("contact lenses")) {
            brandID=99;
        }

        // Assign Image
        ImageView img = (ImageView) view.findViewById(R.id.lstV_rackets_racketImage);
        switch (brandID) {
            case 1:
                img.setImageResource(R.drawable.icon_wilson);
                break;
            case 2:
                img.setImageResource(R.drawable.icon_head);
                break;
            case 3:
                img.setImageResource(R.drawable.icon_yonex);
                break;
            case 4:
                img.setImageResource(R.drawable.icon_babolat);
                break;
            case 5:
                img.setImageResource(R.drawable.icon_donnay);
                break;
            case 6:
                img.setImageResource(R.drawable.icon_dunlop);
                break;
            case 7:
                img.setImageResource(R.drawable.icon_pacific);
                break;
            case 8:
                img.setImageResource(R.drawable.icon_prince);
                break;
            case 9:
                img.setImageResource(R.drawable.icon_solinco);
                break;
            case 10:
                img.setImageResource(R.drawable.icon_tecnifibre);
                break;
            case 11:
                img.setImageResource(R.drawable.icon_voelkl);
                break;
            case 12:
                img.setImageResource(R.drawable.icon_slazenger);
                break;
            case 13:
                img.setImageResource(R.drawable.icon_prokennex);
                break;
            case 14:
                img.setImageResource(R.drawable.icon_artengo);
                break;
            case 15:
                img.setImageResource(R.drawable.icon_ashaway);
                break;
            case 16:
                img.setImageResource(R.drawable.icon_carlton);
                break;
            case 17:
                img.setImageResource(R.drawable.icon_yehlex);
                break;
            case 18:
                img.setImageResource(R.drawable.icon_apacs);
                break;
            case 99:
                img.setImageResource(R.drawable.icon_lenses);
                break;
            default:
                img.setImageResource(R.drawable.icon_nobrand);
                break;
        }

        // === Space for some customization ===

        // Alternating background color
        if (cursor.getPosition() % 2 == 1) {
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.grey));
        } else {
            view.setBackgroundColor(ContextCompat.getColor(view.getContext(), R.color.grey_darker));
        }

    }

}
