/*
 Copyright 2018-2023 Marc C. Keller

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
*/

package com.marc_keller.racketdb;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import android.view.View;
import android.widget.TextView;

/**
 * Created by mck on 27.09.14.
 */
public class RacketSummaryDialog extends AppCompatDialogFragment {

    public int idDB;
    public View view;
    public Racket racket;
    public StringJob stringJob;
    public RacketDbHelper dbHelper;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    static RacketSummaryDialog newInstance(int id) {
        RacketSummaryDialog d = new RacketSummaryDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("racketID", id);
        d.setArguments(args);

        return d;
    }


    @Override
    public AppCompatDialog onCreateDialog(Bundle paramBundle)
    {
        // Get argument
        idDB = getArguments().getInt("racketID");

        // Use Builder to customize dialog
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        //Inflate custom layout and bind to builder
        view = getActivity().getLayoutInflater().inflate(R.layout.racketsummary_dialog,null);
        builder.setView(view);

        // Dialog title
        builder.setTitle(R.string.dialogtitle_racketSummary);

        // get all Information from DB
        dbHelper = new RacketDbHelper(getActivity());
        racket = dbHelper.getRacket(idDB);
        stringJob = dbHelper.getLastStringJobRacket(idDB);


        // Bind Content
        TextView v = (TextView) view.findViewById(R.id.txt_summaryRacket_model);
        v.setText(racket.getModel());

        v = (TextView) view.findViewById(R.id.txt_summaryRacket_owner);
        v.setText(racket.getOwner());

        v = (TextView) view.findViewById(R.id.txt_summaryRacket_number);
        v.setText(Integer.toString(racket.getNumber()));

        v = (TextView) view.findViewById(R.id.txt_summaryRacket_date);
        v.setText(MainActivity.getDateUI(stringJob.getDate()));

        v = (TextView) view.findViewById(R.id.txt_summaryRacket_tension);
        v.setText(String.format("%s %s", stringJob.getTensionNumbers(), stringJob.getTensionUnit()));

        v = (TextView) view.findViewById(R.id.txt_summaryRacket_string);
        v.setText(stringJob.getString());

        v = (TextView) view.findViewById(R.id.txt_summaryRacket_diameter);
        if (Double.isNaN(stringJob.getDiameter())){
            v.setText("-");
        } else {
            v.setText(String.format("%s mm", Double.toString(stringJob.getDiameter())));
        }

        String stringCrosses = stringJob.getStringCrosses();
        if (stringCrosses != null && !stringCrosses.isEmpty()) {
            v = view.findViewById(R.id.txt_summaryRacket_mains);
            v.setVisibility(View.VISIBLE);
            v = view.findViewById(R.id.txt_summaryRacket_mains2);
            v.setVisibility(View.VISIBLE);
            v = view.findViewById(R.id.txt_summaryRacket_crosses);
            v.setVisibility(View.VISIBLE);
            v = view.findViewById(R.id.txt_summaryRacket_crosses2);
            v.setVisibility(View.VISIBLE);

            v = view.findViewById(R.id.txt_summaryRacket_stringCrosses);
            v.setText(stringJob.getStringCrosses());
            v.setVisibility(View.VISIBLE);
            v = view.findViewById(R.id.txt_summaryRacket_diameterCrosses);
            if (Double.isNaN(stringJob.getDiameterCrosses())){
                v.setText("-");
            } else {
                v.setText(String.format("%s mm", Double.toString(stringJob.getDiameterCrosses())));
            }
            v.setVisibility(View.VISIBLE);

        } else {
            v = view.findViewById(R.id.txt_summaryRacket_mains);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.txt_summaryRacket_mains2);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.txt_summaryRacket_crosses);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.txt_summaryRacket_crosses2);
            v.setVisibility(View.GONE);

            v = view.findViewById(R.id.txt_summaryRacket_stringCrosses);
            v.setVisibility(View.GONE);
            v = view.findViewById(R.id.txt_summaryRacket_diameterCrosses);
            v.setVisibility(View.GONE);
        }

        v = (TextView) view.findViewById(R.id.txt_summaryRacket_note);
        String noteContent = stringJob.getNote();
        if(noteContent != null && !noteContent.isEmpty()) {
            v.setText(stringJob.getNote());
        } else {
            v.setText("-");
        }


        // Next Button
        DialogInterface.OnClickListener listenerPositive = new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface paramDialogInt, int paramInt)
            {
                // Start Racket Overview Dialog

                // DialogFragment.show() will take care of adding the fragment
                // in a transaction.  We also want to remove any currently showing
                // dialog, so make our own transaction and take care of that here.
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag("AddStringJobDialog");
                if (prev != null) {
                    ft.remove(prev);
                }
                ft.addToBackStack(null);

                // Create and show the dialog
                //DialogFragment d = AddStringJobDialog.SelectDateDialog.newInstance(idDB);
                AddStringJobDialog.SelectDateDialog d = AddStringJobDialog.SelectDateDialog.newInstance(idDB);
                dismiss(); // first close overview dialog!
                d.show(ft, "AddStringJobDialog");

            }
        };
        builder.setPositiveButton(R.string.add_StringJob,listenerPositive);

        // Cancel Button
        DialogInterface.OnClickListener listenerNegative = new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
            }
        };
        builder.setNegativeButton(R.string.String_Back,listenerNegative);

        // return dialog
        return builder.create();
    }
}